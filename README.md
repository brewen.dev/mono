# Brewen.dev Mono Repo

### Installation

```bash
# Install pnpm (if not already installed)
npm install -g pnpm
# Install dependencies (including scoped packages)
pnpm install
```

### Scripts
* **Start** - Start the built server from the dist folder
```bash
pnpm start:all
pnpm start:front
pnpm start:back
```
* **Serve** - Start the development server
```bash
pnpm serve:all
pnpm serve:front
pnpm serve:back
pnpm serve:front:prod
pnpm serve:back:prod
```
* **Build** - Build the project
```bash
pnpm build:all
pnpm build:front
pnpm build:back
pnpm build:front:prod
pnpm build:back:prod
```
* **Test** - Run the unit tests
```bash
pnpm test:all
pnpm test:front
pnpm test:back
pnpm test:front:watch
pnpm test:back:watch
pnpm test:front:ci
pnpm test:back:ci
```
* **E2E** - Run the end-to-end tests
```bash
pnpm e2e:all
pnpm e2e:front
pnpm e2e:back
pnpm e2e:front:ci
pnpm e2e:back:ci
pnpm e2e:front:prod
pnpm e2e:back:prod
```
* **Lint** - Run the linters
```bash
pnpm lint:all
pnpm lint:front
pnpm lint:back
pnpm lint:front-e2e
pnpm lint:back-e2e
```
* **Format** - Run the formatters and write the changes
```bash
pnpm format
```

<hr/>
<p align="right">
  <a href="LICENSE">GNU GPLv3</a> &copy; 2023 Brewen Couaran
</p>

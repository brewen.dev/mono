import { NgModule } from '@angular/core';
import { NgClass, NgIf } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { ButtonComponent } from './button.component';

@NgModule({
  imports: [FontAwesomeModule, NgClass, NgIf],
  declarations: [ButtonComponent],
  exports: [ButtonComponent],
})
export class ButtonModule {}

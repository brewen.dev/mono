import {
  booleanAttribute,
  Component,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output,
} from '@angular/core';

import { ButtonKind } from './button.types';

@Component({
  selector: 'button',
  styleUrls: ['./button.style.scss'],
  template: `<ng-content></ng-content>`,
})
export class ButtonComponent implements OnInit {
  @Input() kind: ButtonKind = ButtonKind.Primary;

  @Input({
    transform: booleanAttribute,
  })
  rounded = false;
  @Input({
    transform: booleanAttribute,
  })
  disabled = false;

  ngOnInit(): void {
    this.kind = this.kind || ButtonKind.Primary;
  }

  @HostBinding('type') typeAttr = 'button';
  @HostBinding('role') roleAttr = 'button';

  @HostBinding('class')
  get classAttr(): string {
    return [
      'btn',
      `btn-${this.kind}`,
      this.rounded ? 'btn-rounded' : null,
      this.disabled ? 'btn-disabled' : null,
    ]
      .filter(Boolean)
      .join(' ');
  }

  @Output() onClick: EventEmitter<void> = new EventEmitter<void>();

  @HostListener('click')
  onClickHandler(): void {
    if (!this.disabled) {
      this.onClick.emit();
    }
  }
}

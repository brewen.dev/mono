import { Meta, StoryObj } from '@storybook/angular';

import { ButtonComponent } from './button.component';
import { ButtonKind } from './button.types';

const meta: Meta<ButtonComponent> = {
  component: ButtonComponent,
  title: 'Design/Button',
  args: {},
  argTypes: {
    kind: {
      description: "The button's kind",
      options: Object.values(ButtonKind),
      control: 'select',
      type: {
        name: 'enum',
        enum: Object.values(ButtonKind) as string[],
      } as any,
      table: {
        defaultValue: {
          summary: ButtonKind.Primary,
        },
        type: {
          summary: 'ButtonKind',
          detail: Object.values(ButtonKind).join(', '),
        },
      },
    },
    rounded: {
      description: 'Whether the button should be rounded or not',
      control: 'boolean',
      type: 'boolean',
      table: {
        defaultValue: {
          summary: false,
        },
        type: {
          summary: 'boolean',
        },
      },
    },
    disabled: {
      description: 'Whether the button should be disabled or not',
      control: 'boolean',
      type: 'boolean',
      table: {
        defaultValue: {
          summary: false,
        },
        type: {
          summary: 'boolean',
        },
      },
    },
  },
  render: (args) => ({
    props: args,
    template: `
      <button
        [kind]="kind"
        [rounded]="rounded"
        [disabled]="disabled"
      >Button</button>
    `,
  }),
};
export default meta;
type Story = StoryObj<ButtonComponent>;

export const Playground: Story = {};

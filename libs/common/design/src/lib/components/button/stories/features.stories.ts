import { Meta, StoryObj } from '@storybook/angular';

import { ButtonComponent } from '../button.component';

const meta: Meta<ButtonComponent> = {
  component: ButtonComponent,
  title: 'Design/Button/Features',
};
export default meta;
type Story = StoryObj<ButtonComponent>;

// Default

export const Default: Story = {
  render: () => ({
    template: `
      <button>Primary</button>
      <br/>
      <button kind="secondary">
        Secondary
      </button>
      <br/>
      <button kind="tertiary">
        Tertiary
      </button>
      <br/>
      <button kind="success">
        Success
      </button>
      <br/>
      <button kind="danger">
        Danger
      </button>
    `,
  }),
};

// Rounded

export const Rounded: Story = {
  render: () => ({
    template: `
      <button rounded>Primary</button>
      <br/>
      <button kind="secondary" rounded>
        Secondary
      </button>
      <br/>
      <button kind="tertiary" rounded>
        Tertiary
      </button>
      <br/>
      <button kind="success" rounded>
        Success
      </button>
      <br/>
      <button kind="danger" rounded>
        Danger
      </button>
    `,
  }),
};

// Disabled

export const Disabled: Story = {
  render: () => ({
    template: `
      <button disabled>Primary</button>
      <br/>
      <button kind="secondary" disabled>
        Secondary
      </button>
      <br/>
      <button kind="tertiary" disabled>
        Tertiary
      </button>
      <br/>
      <button kind="success" disabled>
        Success
      </button>
      <br/>
      <button kind="danger" disabled>
        Danger
      </button>
    `,
  }),
};

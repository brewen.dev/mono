import { moduleMetadata } from '@storybook/angular';
import {
  FaIconLibrary,
  FontAwesomeModule,
} from '@fortawesome/angular-fontawesome';
import { APP_INITIALIZER } from '@angular/core';

import { faBars } from '@fortawesome/free-solid-svg-icons';

export const IconLibraryDecorator = moduleMetadata({
  imports: [FontAwesomeModule],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: (iconLibrary: FaIconLibrary) => async () => {
        iconLibrary.addIcons(faBars);
      },
      deps: [FaIconLibrary],
      multi: true,
    },
  ],
});

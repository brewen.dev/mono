import { create } from '@storybook/theming/create';

export default create({
  base: 'dark',
  brandTitle: 'Brewen.DEV',
  brandUrl: 'https://www.brewen.dev',
  brandImage: '/branding/header-light.svg',
  brandTarget: '_self',
});

# @brewen.dev/runner

Library for running custom executors & generators.

## Executors

- `@brewen.dev/runner:fly.io` - Deploy project to fly.io ([$schema](./src/executors/fly.io/schema.json))
- `@brewen.dev/runner:nest-build` - Build NestJS project using nest-cli ([$schema](./src/executors/nest-build/schema.json))
- `@brewen.dev/runner:nest-start` - Start NestJS project using nest-cli ([$schema](./src/executors/nest-start/schema.json))

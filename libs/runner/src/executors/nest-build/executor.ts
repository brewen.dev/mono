import type { ExecutorContext } from '@nx/devkit';
import { NestBuildExecutorSchema } from './schema';
import { Executor } from '../utils';

export default (options: NestBuildExecutorSchema, context: ExecutorContext) =>
  runExecutor(
    new Executor<NestBuildExecutorSchema>('nest-build', options, context)
  );

async function runExecutor(executor: Executor<NestBuildExecutorSchema>) {
  executor.normalizeCwd();

  const command = executor.buildCommand(`${executor.get('binary')} build`, {
    config: 'config',
    builder: 'builder',
    'type-check': 'type-check',
  });

  const result = await executor.spawn(command, {
    shell: true,
    stdio: 'pipe',
  });

  if (result.code !== 0) {
    return {
      success: false,
      error: result.stderr.join(''),
    };
  }

  return {
    success: true,
  };
}

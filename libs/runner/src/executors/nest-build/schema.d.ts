export interface NestBuildExecutorSchema {
  binary: string;
  config: string;
  builder: 'tsc' | 'webpack' | 'swc';
  'type-check': boolean;
} // eslint-disable-line

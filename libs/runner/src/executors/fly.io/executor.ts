import type { ExecutorContext } from '@nx/devkit';
import { FlyIoExecutorSchema } from './schema';
import { Executor } from '../utils';

export default (options: FlyIoExecutorSchema, context: ExecutorContext) =>
  runExecutor(new Executor<FlyIoExecutorSchema>('fly.io', options, context));

async function runExecutor(executor: Executor<FlyIoExecutorSchema>) {
  process.chdir(executor.root);

  const command = executor.buildCommand(`${executor.get('binary')} deploy`, {
    config: 'config',
    dockerfile: 'dockerfile',
    'access-token': 'accessToken',
    app: 'app',
  });

  const result = await executor.spawn(command, {
    shell: true,
    stdio: 'pipe',
  });

  return {
    success: result.code === 0,
    error: result.stderr.join(''),
  };
}

export interface FlyIoExecutorSchema {
  accessToken: string;
  app: string;
  config: string;
  dockerfile: string;
  binary: string;
} // eslint-disable-line

import type { ExecutorContext, Task } from '@nx/devkit';
import { spawn as spwn, SpawnOptions } from 'child_process';
import { readFileSync } from 'fs';

export class Executor<Schema> {
  private readonly options: Schema;
  private readonly schema: Record<string, any>;

  constructor(
    private readonly executorName: string,
    options: Schema,
    private readonly _context: ExecutorContext
  ) {
    this.schema = this.loadSchemaConfig();
    this.options = this.parseSchema(options);
  }

  public get(key: keyof Schema): Schema[typeof key] {
    return this.options[key];
  }
  public normalizeCwd() {
    if (this.cwd !== this.projectPath) {
      process.chdir(this.projectPath);
    }
  }

  get context(): ExecutorContext {
    return this._context;
  }

  get root(): string {
    return this.context.root;
  }
  get cwd(): string {
    return this.context.cwd;
  }
  get projectName(): string {
    return this.context.projectName;
  }
  get targetName(): string {
    return this.context.targetName;
  }
  get configurationName(): string | null {
    return this.context.configurationName || null;
  }
  get taskName(): string {
    return `${this.projectName}:${this.targetName}${
      this.configurationName ? `:${this.configurationName}` : ''
    }`;
  }
  get projectRoot(): string {
    return this.currentTask.projectRoot;
  }
  get projectPath(): string {
    return `${this.root}/${this.projectRoot}`;
  }

  get currentTask(): Task {
    return this.context.taskGraph.tasks[this.taskName];
  }

  public spawn(command: string, options: SpawnOptions) {
    return new Promise<SpawnResult>((resolve, reject) => {
      const cp = spwn(command, options),
        stderr: string[] = [],
        stdout: string[] = [];

      cp.stdout.on('data', (data) => {
        stdout.push(data.toString());
        console.log(data.toString());
      });

      cp.stderr.on('data', (data) => {
        stderr.push(data.toString());
        console.error(data.toString());
      });

      cp.on('close', (code) => {
        if (code === 0) {
          resolve({
            code,
            stdout,
            stderr,
          });
        } else {
          reject(stderr.join(''));
        }
      });
    });
  }

  public buildCommand(command: string, schema: Record<string, string>) {
    const optionsList = Object.keys(schema)
      .map((key) => {
        const schemaKey = schema[key];
        const value = this.options[schemaKey];

        switch (typeof value) {
          case 'boolean':
            return value ? `--${key}` : null;
          case 'string':
            return `--${key}=${value}`;
          default:
            return null;
        }
      })
      .filter(Boolean);

    return `${command} ${optionsList.join(' ')}`;
  }

  private parseSchema(options: Schema): Schema {
    const replacedOptions = JSON.parse(
      JSON.stringify(options)
        .replace(/\$(\w+)/g, (_, key) => process.env[key] || '')
        .replace(/'/g, '"')
    ) as Schema;

    Object.keys(replacedOptions).forEach((key) => {
      const value = replacedOptions[key];
      if (!value) {
        replacedOptions[key] = this.schema.properties?.[key]?.default;
      }
    });

    return replacedOptions;
  }

  private loadSchemaConfig() {
    const path = `${__dirname}/${this.executorName}/schema.json`;
    const content = readFileSync(path).toString();
    return JSON.parse(content);
  }
}

type SpawnResult = {
  code: number;
  stdout: string[];
  stderr: string[];
};

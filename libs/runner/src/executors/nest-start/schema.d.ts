export interface NestStartExecutorSchema {
  binary: string;
  config: string;
  builder: 'tsc' | 'webpack' | 'swc';
  watch: boolean;
  entryFile: string;
  exec: string;
  'type-check': boolean;
} // eslint-disable-line

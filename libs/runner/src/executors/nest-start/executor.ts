import type { ExecutorContext } from '@nx/devkit';
import { Executor } from '../utils';
import { NestStartExecutorSchema } from './schema';

export default (options: NestStartExecutorSchema, context: ExecutorContext) =>
  runExecutor(
    new Executor<NestStartExecutorSchema>('nest-start', options, context)
  );

async function runExecutor(executor: Executor<NestStartExecutorSchema>) {
  executor.normalizeCwd();

  const command = executor.buildCommand(`${executor.get('binary')} start`, {
    config: 'config',
    builder: 'builder',
    watch: 'watch',
    entryFile: 'entryFile',
    exec: 'exec',
    'type-check': 'type-check',
  });

  const result = await executor.spawn(command, {
    shell: true,
    stdio: 'pipe',
  });

  if (result.code !== 0) {
    return {
      success: false,
      error: result.stderr.join(''),
    };
  }

  return {
    success: true,
  };
}

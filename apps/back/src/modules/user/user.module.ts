import { Logger, Module, Provider } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CqrsModule } from '@nestjs/cqrs';

import { User, UserSchema } from './database/models/user.model';
import { Repositories } from './database/repositories';
import { UserMapper } from './user.mapper';

import { CurrentHttpController } from './queries/current/current.http.controller';

import { GetUserHttpController } from './queries/get-user/get-user.http.controller';
import { GetUserQueryHandler } from './queries/get-user/get-user.query-handler';

import { ListUsersHttpController } from './queries/list-users/list-users.http.controller';
import { ListUsersQueryHandler } from './queries/list-users/list-users.query-handler';

const HttpControllers = [
  CurrentHttpController,
  GetUserHttpController,
  ListUsersHttpController,
];
const MessageControllers = [];
const CliControllers = [];

const CommandHandlers: Provider[] = [];
const QueryHandlers: Provider[] = [GetUserQueryHandler, ListUsersQueryHandler];

const Mappers: Provider[] = [UserMapper];

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: User.name,
        schema: UserSchema,
      },
    ]),

    CqrsModule,
  ],
  controllers: [...HttpControllers, ...MessageControllers],
  providers: [
    Logger,
    ...CliControllers,
    ...Repositories,
    ...CommandHandlers,
    ...QueryHandlers,
    ...Mappers,
  ],
})
export class UserModule {}

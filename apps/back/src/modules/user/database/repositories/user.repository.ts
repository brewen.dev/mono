import { Injectable, Logger } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { MongoRepositoryBase } from '@libs/db/mongo-repository.base';
import { Paginated, PaginatedQueryParams } from '@libs/ddd';

import { UserEntity } from '../../domain/user.entity';
import { UserMapper } from '../../user.mapper';
import { IUser, User } from '../models/user.model';
import { UserRepositoryPort } from './user.repository.port';

@Injectable()
export class UserRepository
  extends MongoRepositoryBase<UserEntity, IUser>
  implements UserRepositoryPort
{
  constructor(
    @InjectModel(User.name) protected readonly model: Model<User>,
    mapper: UserMapper,
    eventEmitter: EventEmitter2
  ) {
    super(mapper, eventEmitter, new Logger(UserRepository.name));
  }

  async findOneByEmail(email: string): Promise<UserEntity | null> {
    const user = await this.model.findOne({ email }).exec();

    if (!user) {
      return null;
    }

    return this.mapper.toDomain(user);
  }

  async findOneByUsername(username: string): Promise<UserEntity | null> {
    const user = await this.model
      .findOne({ username: { $regex: `^${username}$`, $options: 'i' } })
      .exec();

    if (!user) {
      return null;
    }

    return this.mapper.toDomain(user);
  }

  async search(
    key: keyof User,
    query: string,
    params: PaginatedQueryParams
  ): Promise<Paginated<UserEntity>> {
    return this.findPaginated(params, [
      {
        $match: {
          [key]: { $regex: query, $options: 'i' },
        },
      },
    ]);
  }
}

import { Paginated, PaginatedQueryParams, RepositoryPort } from '@libs/ddd';

import { UserEntity } from '../../domain/user.entity';
import { User } from '../models/user.model';

export interface UserRepositoryPort extends RepositoryPort<UserEntity> {
  findOneByEmail(email: string): Promise<UserEntity | null>;
  findOneByUsername(username: string): Promise<UserEntity | null>;

  search(
    key: keyof User,
    query: string,
    params: PaginatedQueryParams
  ): Promise<Paginated<UserEntity>>;
}

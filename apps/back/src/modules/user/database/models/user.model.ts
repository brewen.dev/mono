import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

import { DefaultSchema } from '@libs/db/schema.base';
import { Guard } from '@libs/guard';
import { ObjectLiteral } from '@libs/types';

import { MongoCollections } from '@configs/mongo.collections';
import { ValidationOptions } from '@configs/app.const';

import { UserRoles } from '@modules/user/domain/user.types';

@Schema({
  ...DefaultSchema,
  collection: MongoCollections.Users,
})
export class User {
  _id: Types.ObjectId;
  createdAt: Date;
  updatedAt: Date;

  @Prop({
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: (value: string) => Guard.isUsername(value),
    },
  })
  username: string;

  @Prop({
    type: String,
    default: () => (this as any).username,
    validate: {
      validator: (value: string) =>
        Guard.lengthIsBetween(
          value,
          ValidationOptions.displayName.minLength,
          ValidationOptions.displayName.maxLength
        ),
    },
  })
  displayName: string;

  @Prop({
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: (value: string) => Guard.isEmail(value),
    },
  })
  email: string;

  @Prop({
    type: [String],
    required: true,
    validate: {
      validator: (values: string[]) =>
        Guard.each(values, (value) => Guard.isEnum(value, UserRoles)),
    },
  })
  roles: UserRoles[];
}

export type UserDocument = User & Document;
export const UserSchema = SchemaFactory.createForClass(User);

export interface IUser extends ObjectLiteral {
  _id: Types.ObjectId;
  createdAt: Date;
  updatedAt: Date;

  username: string;
  displayName: string;
  email: string;
  roles: UserRoles[];
}

export const SortableUserFields: {
  [key in keyof IUser]?: string;
} = {
  createdAt: 'createdAt',
  updatedAt: 'updatedAt',
  username: 'username',
  displayName: 'displayName',
};
export type SortableUserField = (typeof SortableUserFields)[number];

import { ApiProperty } from '@nestjs/swagger';
import { ResponseBase } from '@libs/api/response.base';

import { UserRoles } from '../domain/user.types';

export class PublicUserResponseDto extends ResponseBase {
  @ApiProperty({
    example: '64fe2648dad36a6bba33fa72',
    description: 'The id of the user',
  })
  id: string;

  @ApiProperty({
    example: 'john.doe',
    description: 'The username of the user',
  })
  username: string;

  @ApiProperty({
    example: 'John Doe',
    description: 'The display name of the user',
  })
  displayName: string;

  @ApiProperty({
    example: [UserRoles.Member],
    description: 'The roles of the user',
    enum: UserRoles,
  })
  roles: UserRoles[];
}

export class UserResponseDto extends PublicUserResponseDto {
  @ApiProperty({
    example: 'john.doe@example.com',
    description: 'The email of the user',
  })
  email: string;
}

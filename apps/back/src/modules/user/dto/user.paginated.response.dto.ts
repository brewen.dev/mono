import { ApiProperty } from '@nestjs/swagger';
import { PaginatedResponseDto } from '@libs/api/paginated.response.base';

import { PublicUserResponseDto } from './user.response.dto';

export class UserPaginatedResponseDto extends PaginatedResponseDto<PublicUserResponseDto> {
  @ApiProperty({
    type: PublicUserResponseDto,
    isArray: true,
    description: 'The list of users',
  })
  readonly data: readonly PublicUserResponseDto[];
}

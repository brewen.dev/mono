import { AggregateID, AggregateRoot } from '@libs/ddd';
import { Encrypt } from '@libs/utils/encrypt';

import { AuthType } from '@modules/auth/domain/auth.types';

import { UserCreatedDomainEvent } from './events/user-created.domain-event';
import { CreateUserProps, UserProps, UserRoles } from './user.types';

export class UserEntity extends AggregateRoot<UserProps> {
  protected readonly _id: AggregateID;

  static create(create: CreateUserProps): UserEntity {
    const id = AggregateID.create();
    const props: UserProps = {
      ...create,
      displayName: create.username,
      roles: [UserRoles.Member],
    };

    const user = new UserEntity({ id, props });

    user.addEvent(
      new UserCreatedDomainEvent({
        aggregateId: id,
        username: create.username,
        email: create.email,
      })
    );

    return user;
  }

  comparePassword(password: string): boolean {
    if (this.props.auth.type !== AuthType.Local || !this.props.auth.local) {
      return false;
    }

    return Encrypt.compare(password, this.props.auth.local.password);
  }

  validate() {}
}

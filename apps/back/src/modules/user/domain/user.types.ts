import { Auth } from '@modules/auth/domain/value-objects/auth.value-object';

export interface UserProps {
  username: string;
  displayName: string;
  email: string;
  roles: UserRoles[];
  auth?: Auth;
}

export interface CreateUserProps {
  username: string;
  email: string;
  auth: Auth;
}

export enum UserRoles {
  Member = 'MEMBER',
  Administrator = 'ADMINISTRATOR',
}

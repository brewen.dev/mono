import { Controller, Get, HttpStatus, Query } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { QueryBus } from '@nestjs/cqrs';
import { Result } from 'oxide.ts';

import { SwaggerTags } from '@configs/app.config';
import { RoutesV1 } from '@configs/app.routes';

import { ApiErrorResponse } from '@libs/api/api-error.response';
import { Paginated } from '@libs/ddd';

import { UserMapper } from '../../user.mapper';
import { UserPaginatedResponseDto } from '../../dto/user.paginated.response.dto';
import { UserEntity } from '../../domain/user.entity';

import { ListUsersRequestDto } from './list-users.request.dto';
import { ListUsersQuery } from './list-users.query-handler';

@ApiTags(SwaggerTags.User)
@Controller(RoutesV1.version)
export class ListUsersHttpController {
  constructor(
    private readonly queryBus: QueryBus,
    private readonly mapper: UserMapper
  ) {}

  @ApiOperation({
    summary: 'List users',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The list of users, paginated by the given parameters',
    type: UserPaginatedResponseDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'The given parameters are invalid',
    type: ApiErrorResponse,
  })
  @Get(RoutesV1.user.get.list)
  async listUsers(
    @Query() queryParams: ListUsersRequestDto
  ): Promise<UserPaginatedResponseDto> {
    const query = new ListUsersQuery(queryParams);

    const result: Result<
      Paginated<UserEntity>,
      Error
    > = await this.queryBus.execute(query);

    const paginated = result.unwrap();

    return new UserPaginatedResponseDto({
      ...paginated,
      data: paginated.data.map((user) => this.mapper.toPublicResponse(user)),
    });
  }
}

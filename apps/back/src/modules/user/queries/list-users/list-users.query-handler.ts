import { Inject } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { Ok, Result } from 'oxide.ts';

import { PaginatedParams, PaginatedQueryBase } from '@libs/ddd/query.base';
import { Paginated } from '@libs/ddd';

import { UserEntity } from '../../domain/user.entity';
import { USER_REPOSITORY } from '../../user.tokens';
import { UserRepositoryPort } from '../../database/repositories/user.repository.port';

export class ListUsersQuery extends PaginatedQueryBase {
  readonly username?: string;
  readonly search?: string;

  constructor(props: PaginatedParams<ListUsersQuery>) {
    super(props);
    this.username = props.username;
    this.search = props.search;
  }
}

@QueryHandler(ListUsersQuery)
export class ListUsersQueryHandler implements IQueryHandler {
  constructor(
    @Inject(USER_REPOSITORY)
    private readonly userRepository: UserRepositoryPort
  ) {}

  async execute(
    query: ListUsersQuery
  ): Promise<Result<Paginated<UserEntity>, Error>> {
    const { username, search, ...params } = query;

    if (username) {
      const user = await this.userRepository.findOneByUsername(username);
      return Ok(
        new Paginated<UserEntity>({
          data: user ? [user] : [],
          count: 1,
          page: 1,
          limit: params.limit,
        })
      );
    }

    if (search) {
      const users = await this.userRepository.search(
        'username',
        search,
        params
      );
      return Ok(users);
    }

    const users = await this.userRepository.findAllPaginated(params);
    return Ok(users);
  }
}

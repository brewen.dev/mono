import { ApiProperty } from '@nestjs/swagger';
import {
  IsEnum,
  IsOptional,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';

import { ValidationOptions } from '@configs/app.const';
import { PaginatedQueryRequestDto } from '@libs/api/paginated-query.request.dto';

import {
  SortableUserField,
  SortableUserFields,
} from '../../database/models/user.model';

export class ListUsersRequestDto extends PaginatedQueryRequestDto {
  @ApiProperty({
    example: 'john.doe',
    description:
      'Get a single user with a given username (page & limit will be ignored)',
    required: false,
  })
  @IsOptional()
  @MinLength(ValidationOptions.username.minLength)
  @MaxLength(ValidationOptions.username.maxLength)
  @Matches(ValidationOptions.username.match)
  @IsString()
  readonly username: string;

  @ApiProperty({
    example: 'john',
    description: 'Search for a username, will be ignored if username is given',
    required: false,
  })
  @IsOptional()
  @MinLength(1)
  @MaxLength(
    Math.max(
      ValidationOptions.displayName.maxLength,
      ValidationOptions.username.maxLength
    )
  )
  @IsString()
  readonly search: string;

  @ApiProperty({
    example: SortableUserFields.username,
    description: 'Specifies the field to sort by',
    required: false,
    default: SortableUserFields.updatedAt,
    enum: SortableUserFields,
  })
  @IsOptional()
  @IsEnum(SortableUserFields)
  @IsString()
  readonly orderBy: SortableUserField = SortableUserFields.updatedAt;
}

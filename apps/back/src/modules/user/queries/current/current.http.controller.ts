import { Controller, Get, HttpStatus, Req, UseGuards } from '@nestjs/common';
import {
  ApiCookieAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Request } from 'express';

import { RoutesV1 } from '@configs/app.routes';
import { SwaggerTags } from '@configs/app.config';

import { ApiErrorResponse } from '@libs/api/api-error.response';
import { AuthenticatedGuard } from '@libs/guards/auth.guard';

import { UserMapper } from '../../user.mapper';
import { UserResponseDto } from '../../dto/user.response.dto';
import { UserEntity } from '../../domain/user.entity';

@ApiTags(SwaggerTags.User)
@Controller(RoutesV1.version)
export class CurrentHttpController {
  constructor(private readonly mapper: UserMapper) {}

  @ApiOperation({ summary: 'Get current authenticated user' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Current authenticated user',
    type: UserResponseDto,
  })
  @ApiResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Invalid or expired session token',
    type: ApiErrorResponse,
  })
  @ApiCookieAuth('session')
  @UseGuards(AuthenticatedGuard)
  @Get(RoutesV1.user.get.current)
  async getCurrentUser(@Req() request: Request): Promise<UserResponseDto> {
    return this.mapper.toResponse(request.user as UserEntity);
  }
}

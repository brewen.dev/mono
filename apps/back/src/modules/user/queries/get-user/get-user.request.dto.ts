import { ApiProperty } from '@nestjs/swagger';
import { IsMongoId, Length } from 'class-validator';
import { ValidationOptions } from '@configs/app.const';

export class GetUserRequestDto {
  @ApiProperty({
    description: 'The id of the user to get',
    example: '64fe2648dad36a6bba33fa72',
    pattern: ValidationOptions.id.match.toString(),
  })
  @Length(ValidationOptions.id.length)
  @IsMongoId()
  readonly id: string;
}

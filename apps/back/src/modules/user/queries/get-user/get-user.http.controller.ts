import {
  Controller,
  Get,
  HttpStatus,
  NotFoundException as NotFoundHttpException,
  Param,
} from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { QueryBus } from '@nestjs/cqrs';
import { match, Result } from 'oxide.ts';

import { RoutesV1 } from '@configs/app.routes';
import { SwaggerTags } from '@configs/app.config';

import { ApiErrorResponse } from '@libs/api/api-error.response';

import { UserMapper } from '../../user.mapper';
import { PublicUserResponseDto } from '../../dto/user.response.dto';
import { UserEntity } from '../../domain/user.entity';
import { UserNotFoundError } from '../../domain/user.errors';

import { GetUserRequestDto } from './get-user.request.dto';
import { GetUserQuery } from './get-user.query-handler';

@ApiTags(SwaggerTags.User)
@Controller(RoutesV1.version)
export class GetUserHttpController {
  constructor(
    private readonly queryBus: QueryBus,
    private readonly mapper: UserMapper
  ) {}

  @ApiOperation({ summary: 'Get user by id' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'User by id',
    type: PublicUserResponseDto,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'User not found',
    type: ApiErrorResponse,
  })
  @Get(RoutesV1.user.get.get)
  async getUser(
    @Param() params: GetUserRequestDto
  ): Promise<PublicUserResponseDto> {
    const query = new GetUserQuery(params);

    const result: Result<UserEntity, Error> = await this.queryBus.execute(
      query
    );

    return match(result, {
      Ok: (user: UserEntity) => this.mapper.toPublicResponse(user),
      Err: (error: Error): any => {
        if (error instanceof UserNotFoundError) {
          throw new NotFoundHttpException(error.message);
        }
        throw error;
      },
    });
  }
}

import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { Err, Ok, Result } from 'oxide.ts';

import { Params, QueryBase } from '@libs/ddd/query.base';
import { UserRepository } from '../../database/repositories/user.repository';
import { UserEntity } from '@modules/user/domain/user.entity';
import { UserNotFoundError } from '@modules/user/domain/user.errors';
import { Inject } from '@nestjs/common';
import { USER_REPOSITORY } from '@modules/user/user.tokens';

export class GetUserQuery extends QueryBase {
  readonly id: string;

  constructor(props: Params<GetUserQuery>) {
    super();
    this.id = props.id;
  }
}

@QueryHandler(GetUserQuery)
export class GetUserQueryHandler implements IQueryHandler {
  constructor(
    @Inject(USER_REPOSITORY) private readonly userRepository: UserRepository
  ) {}

  async execute(query: GetUserQuery): Promise<Result<UserEntity, Error>> {
    const user = await this.userRepository.findOneById(query.id);

    if (user.isSome()) {
      return Ok(user.unwrap());
    }

    return Err(
      new UserNotFoundError(undefined, {
        id: query.id,
      })
    );
  }
}

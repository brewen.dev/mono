import { Injectable } from '@nestjs/common';

import { Mapper } from '@libs/ddd';
import { MongoUtils } from '@libs/utils/mongo.util';

import { UserEntity } from './domain/user.entity';
import {
  PublicUserResponseDto,
  UserResponseDto,
} from './dto/user.response.dto';
import { IUser } from '@modules/user/database/models/user.model';

@Injectable()
export class UserMapper implements Mapper<UserEntity, IUser, UserResponseDto> {
  toPersistence(entity: UserEntity): IUser {
    const copy = entity.getProps();

    return {
      _id: MongoUtils.toObjectId(copy.id),
      createdAt: copy.createdAt,
      updatedAt: copy.updatedAt,

      username: copy.username,
      displayName: copy.displayName,
      email: copy.email,
      roles: copy.roles,
    };
  }

  toDomain(record: IUser): UserEntity {
    return new UserEntity({
      id: MongoUtils.fromObjectId(record._id),
      createdAt: record.createdAt,
      updatedAt: record.updatedAt,
      props: {
        username: record.username,
        displayName: record.displayName,
        email: record.email,
        roles: record.roles,
      },
    });
  }

  toResponse(entity: UserEntity): UserResponseDto {
    const props = entity.getProps();
    const response = new UserResponseDto(entity);
    response.id = props.id;
    response.username = props.username;
    response.displayName = props.displayName;
    response.email = props.email;
    response.roles = props.roles;
    return response;
  }

  toPublicResponse(entity: UserEntity): PublicUserResponseDto {
    const props = entity.getProps();
    const response = new PublicUserResponseDto(entity);
    response.id = props.id;
    response.username = props.username;
    response.displayName = props.displayName;
    response.roles = props.roles;
    return response;
  }
}

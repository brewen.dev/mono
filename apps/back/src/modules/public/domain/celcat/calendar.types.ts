import { CalendarType } from '../celcat';

export type CalendarConfig = {
  id: string;
  type: CalendarType;
};

export const FULL_DAY_IN_HOURS = 12.5; // Full school day is 12.5 hours

export type Calendar = {
  prodid: string;
  version: string;
  x_wr_calname: string;
  x_published_ttl: string;
  method?: string;

  events: CalendarEvent[];
  extra: string[];
};

export type CalendarEvent = {
  uid: string;
  dtstamp: string;
  dtstart: string;
  dtend: string;
  location: string;
  summary: string;
  description: string;
  sequence: string;

  color?: string;
  category?: string;
  extra: string[];
};

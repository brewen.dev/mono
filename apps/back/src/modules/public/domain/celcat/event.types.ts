import { CalendarType } from './celcat.types';

export enum EventCategories {
  Cours = 'Cours',
  TD = 'TD',
  TD_Machine = 'TD Machine',
  CoursTD = 'Cours/TD',
  Atelier = 'Atelier',

  ReunionDeRentree = 'Réunion de rentrée',
  Vacances = 'Vacances',
  Autre = 'Autre',
}

export const EventCategoriesColors: Record<
  EventCategories,
  {
    default: string;
    [CalendarType.Outlook]?: string;
  }
> = {
  [EventCategories.Cours]: {
    default: 'aqua',
    [CalendarType.Outlook]: 'Blue Category',
  },
  [EventCategories.TD]: {
    default: 'lightgreen',
    [CalendarType.Outlook]: 'Green Category',
  },
  [EventCategories.TD_Machine]: {
    default: 'darkred',
    [CalendarType.Outlook]: 'Red Category',
  },
  [EventCategories.CoursTD]: {
    default: 'darkmagenta',
    [CalendarType.Outlook]: 'Purple Category',
  },
  [EventCategories.Atelier]: {
    default: 'darkgoldenrod',
    [CalendarType.Outlook]: 'Orange Category',
  },

  [EventCategories.ReunionDeRentree]: {
    default: 'darkgoldenrod',
    [CalendarType.Outlook]: 'Orange Category',
  },
  [EventCategories.Vacances]: {
    default: 'crimson',
    [CalendarType.Outlook]: 'Red Category',
  },
  [EventCategories.Autre]: {
    default: 'white',
    [CalendarType.Outlook]: 'Yellow Category',
  },
};

export const EventArguments = [
  'Event category',
  'Module',
  'Groups',
  'Group',
  'Staff',
  'Room',
  'Note',
] as const;
export type EventArgument = (typeof EventArguments)[number];
export const EventArgumentDefaults: Record<EventArgument, string> = {
  'Event category': 'Autre',
  Module: '',
  Groups: '',
  Group: '',
  Staff: 'None',
  Room: 'None',
  Note: '',
} as const;
export const EventArgumentAliases: Record<EventArgument, string[]> = {
  'Event category': ['Category', 'Type'],
  Module: ['Module', 'Subject'],
  Groups: ['Groups', 'Groupes'],
  Group: ['Group', 'Groupe'],
  Staff: ['Staff', 'Professeur', 'Teacher'],
  Room: ['Room', 'Salle'],
  Note: ['Note', 'Notes'],
};

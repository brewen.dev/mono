export enum Tokens {
  BeginCalendar = 'BEGIN:VCALENDAR',
  EndCalendar = 'END:VCALENDAR',
  BeginEvent = 'BEGIN:VEVENT',
  EndEvent = 'END:VEVENT',
  ProdID = 'PRODID',
  Version = 'VERSION',
  CalName = 'X-WR-CALNAME',
  Timezone = 'X-WR-TIMEZONE',
  PublishedTTL = 'X-PUBLISHED-TTL',
  Method = 'METHOD',
  UID = 'UID',
  DTStamp = 'DTSTAMP',
  DTStart = 'DTSTART',
  DTEnd = 'DTEND',
  Location = 'LOCATION',
  Summary = 'SUMMARY',
  Description = 'DESCRIPTION',
  Sequence = 'SEQUENCE',
  Color = 'COLOR',
  Categories = 'CATEGORIES',

  Colon = ':',
  Multiline = ' ',
  DescriptionDelimiter = ': ',
}

export const EscapedTokens: [string, string][] = [
  ['\\n', '\n'],
  ['\\,', ','],
  ['\\;', ';'],
];

export enum CalendarType {
  Default = 'default',
  Google = 'google',
  Outlook = 'outlook',
  Proton = 'proton',
}

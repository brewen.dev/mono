import { CalendarType } from './celcat.types';
import { CalendarTimezone } from './timezone.types';

export type CalendarTypeConfig = {
  readonly timezone: CalendarTimezone;
  readonly useHtml: boolean;
  readonly useColors: boolean;
};
export type TypedCalendarTypeConfig = CalendarTypeConfig & {
  readonly type: CalendarType;
};

export const CalendarTypeConfigs: Record<CalendarType, CalendarTypeConfig> = {
  [CalendarType.Default]: {
    timezone: CalendarTimezone.FR,
    useHtml: false,
    useColors: false,
  },
  [CalendarType.Google]: {
    timezone: CalendarTimezone.FR,
    useHtml: true,
    useColors: true,
  },
  [CalendarType.Outlook]: {
    timezone: CalendarTimezone.FR,
    useHtml: false,
    useColors: true,
  },
  [CalendarType.Proton]: {
    timezone: CalendarTimezone.FR,
    useHtml: true,
    useColors: false,
  },
};

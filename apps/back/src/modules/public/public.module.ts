import { Logger, Module, Provider } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';

import { CelcatHttpController } from './queries/celcat/celcat.http.controller';
import { CelcatQueryHandler } from './queries/celcat/celcat.query-handler';
import { CelcatService } from './queries/celcat/celcat.service';

const HttpControllers = [CelcatHttpController];

const Services: Provider[] = [CelcatService];
const QueryHandlers: Provider[] = [CelcatQueryHandler];

@Module({
  imports: [CqrsModule],
  controllers: [...HttpControllers],
  providers: [Logger, ...Services, ...QueryHandlers],
})
export class PublicModule {}

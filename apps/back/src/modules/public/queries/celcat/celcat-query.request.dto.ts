import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsEnum, IsOptional } from 'class-validator';
import { Type } from 'class-transformer';

import { CalendarTimezone } from '@modules/public/domain/celcat';

export class CelcatQueryRequestDto {
  @ApiProperty({
    example: 'Europe/Paris',
    description: 'Specifies the timezone for the calendar',
    required: false,
    enum: CalendarTimezone,
  })
  @IsOptional()
  @IsEnum(CalendarTimezone)
  @Type(() => String)
  readonly timezone?: CalendarTimezone;

  @ApiProperty({
    example: true,
    description:
      'Specifies if the calendar should use HTML in the description of events (bold, italic, etc.)',
    required: false,
  })
  @IsOptional()
  @IsBoolean()
  @Type(() => Boolean)
  readonly useHtml?: boolean;

  @ApiProperty({
    example: true,
    description:
      'Specifies if the calendar should use colors for different types of events (may or may not be supported by your calendar app)',
    required: false,
  })
  @IsOptional()
  @IsBoolean()
  @Type(() => Boolean)
  readonly useColors?: boolean;
}

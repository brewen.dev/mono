import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString, Length } from 'class-validator';
import { Type } from 'class-transformer';

import { CalendarType } from '@modules/public/domain/celcat';

export class CelcatParamsRequestDto {
  @ApiProperty({
    example: '12D45ES8F4S7D87',
    description:
      'Specifies the calendar id provided by Celcat in the url, like so:\nhttps://celcat.u-bordeaux.fr/ICalFeed/ical/%id%/schedule.ics',
    required: true,
  })
  @IsString()
  @Length(10, 20)
  @Type(() => String)
  readonly id: string;

  @ApiProperty({
    enum: CalendarType,
    example: CalendarType.Default,
    description: 'Specifies the calendar type for default configuration',
    required: false,
  })
  @IsOptional()
  @IsEnum(CalendarType)
  @Type(() => String)
  readonly type?: CalendarType;
}

import {
  Calendar,
  CalendarEvent,
  CalendarType,
  EventArgument,
  EventArgumentAliases,
  EventArgumentDefaults,
  EventArguments,
  EventCategoriesColors,
  FULL_DAY_IN_HOURS,
  TypedCalendarTypeConfig,
} from '@modules/public/domain/celcat';
import { CelcatParser } from './celcat.parser';

export class CelcatFormatter {
  constructor(private readonly $parser: CelcatParser) {}

  public formatCalendar(
    calendar: Calendar,
    config: TypedCalendarTypeConfig
  ): Calendar {
    return {
      ...calendar,
      events: this._processEvents(calendar.events, config),
    };
  }

  private _processEvents(
    events: CalendarEvent[],
    config: TypedCalendarTypeConfig
  ): CalendarEvent[] {
    let skip = 0;

    return events
      .map((event) => {
        if (skip > 0) {
          skip--;
          return null;
        }

        const argv = this.$parser.parseEventArguments(event);

        if (this._isEventAllDay(event)) {
          event.dtstart = `;VALUE=DATE:${this._truncateDate(event.dtstart)}`;
          event.dtend = `;VALUE=DATE:${this._truncateDate(event.dtend)}`;
        }

        event.summary = `[${argv['Event category'] || 'Autre'}] ${
          argv['Module'] || ''
        }`;

        event.description = this._formatDescription(argv, config.useHtml);

        if (config.useColors) {
          const color = EventCategoriesColors[argv['Event category']],
            value = color?.[config.type] ?? color?.default;

          if (value) {
            if (config.type === CalendarType.Outlook) {
              event.category = value;
            } else {
              event.color = value;
            }
          }
        }

        return event;
      })
      .filter((event) => event !== null);
  }

  private _formatDescription(args: any, useHtml: boolean): string {
    const lines = [];

    EventArguments.forEach((key) => {
      const value = this._getArgument(args, key, useHtml);
      if (value) {
        lines.push(value);
      }
    });

    Object.keys(args)
      .filter((key: any) => !EventArguments.includes(key) && key !== 'Event id')
      .forEach((key) => {
        const value = this._formatArgument(key, args[key], useHtml);
        if (value) {
          lines.push(value);
        }
      });

    lines.push(
      '',
      useHtml
        ? `<i>Event id: ${args['Event id']}</i>`
        : `Event id: ${args['Event id']}`
    );

    return lines.join(useHtml ? '<br>' : '\n');
  }
  private _getArgument(
    args: any,
    key: EventArgument,
    useHtml: boolean
  ): string {
    const aliases = EventArgumentAliases[key];
    let value = args[key] ?? aliases.find((alias) => args[alias] !== undefined);

    if (value !== undefined && !value) {
      value = EventArgumentDefaults[key];
    }

    return this._formatArgument(aliases[0] || key, value, useHtml);
  }

  private _formatArgument(
    key: string,
    value: string,
    useHtml: boolean
  ): string {
    if (!value) {
      return '';
    }

    if (useHtml) {
      return `<b>${key}</b>: ${value}`;
    }

    return `${key}: ${value}`;
  }

  private _truncateDate(date: string): string {
    return date.split('T')[0].replace(/-/g, '');
  }

  private _isEventAllDay(event: CalendarEvent): boolean {
    const start = this.$parser.parseICSDate(event.dtstart, true);
    const end = this.$parser.parseICSDate(event.dtend, true);
    const length = end.getTime() - start.getTime();

    return length >= FULL_DAY_IN_HOURS * 60 * 60 * 1000;
  }
}

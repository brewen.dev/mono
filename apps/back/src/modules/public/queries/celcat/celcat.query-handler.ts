import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { Ok, Result } from 'oxide.ts';

import { Params, QueryBase } from '@libs/ddd/query.base';

import {
  CalendarConfig,
  CalendarType,
  CalendarTypeConfigs,
  CalendarTypeConfig,
} from '@modules/public/domain/celcat';

import { CelcatService } from '@modules/public/queries/celcat/celcat.service';

export class CelcatQuery extends QueryBase {
  readonly config: CalendarConfig;
  readonly typeConfig: CalendarTypeConfig;

  constructor(props: Params<CelcatQuery>) {
    super();
    this.config = props.config;

    const defaultTypeConfig =
      CalendarTypeConfigs[this.config.type] ??
      CalendarTypeConfigs[CalendarType.Default];

    this.typeConfig = {
      ...defaultTypeConfig,
      ...JSON.parse(JSON.stringify(props.typeConfig)),
    };
  }
}

@QueryHandler(CelcatQuery)
export class CelcatQueryHandler implements IQueryHandler {
  constructor(private readonly $service: CelcatService) {}

  async execute(query: CelcatQuery): Promise<Result<string, Error>> {
    const ics = await this.$service.getCalendar(query);
    return Ok(ics);
  }
}

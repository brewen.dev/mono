import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Query,
  Response,
} from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Response as Res } from 'express';
import { Result } from 'oxide.ts';

import { RoutesV1 } from '@configs/app.routes';
import { SwaggerTags } from '@configs/app.config';
import { ApiErrorResponse } from '@libs/api/api-error.response';
import { CalendarType } from '@modules/public/domain/celcat';

import { CelcatParamsRequestDto } from './celcat-params.request.dto';
import { CelcatQueryRequestDto } from './celcat-query.request.dto';
import { CelcatQuery } from './celcat.query-handler';

@ApiTags(SwaggerTags.Public)
@Controller(RoutesV1.version)
export class CelcatHttpController {
  constructor(private readonly queryBus: QueryBus) {}

  @Get(RoutesV1.public.celcat)
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Get formatted Celcat ICS file' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Returns the formatted Celcat ICS file',
    type: 'text/calendar',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ApiErrorResponse,
  })
  async getCelcatICS(
    @Param() params: CelcatParamsRequestDto,
    @Query() queryParams: CelcatQueryRequestDto,
    @Response() response: Res
  ): Promise<any> {
    const query = new CelcatQuery({
      config: {
        id: params.id,
        type: params.type ?? CalendarType.Default,
      },
      typeConfig: {
        timezone: queryParams.timezone,
        useHtml: queryParams.useHtml,
        useColors: queryParams.useColors,
      },
    });
    const result: Result<string, Error> = await this.queryBus.execute(query);

    return response
      .header('Content-Type', 'text/calendar')
      .send(result.unwrap());
  }
}

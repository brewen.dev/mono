import { Logger } from '@nestjs/common';

import {
  Calendar,
  CalendarEvent,
  EscapedTokens,
  CalendarTimezone,
  Tokens,
} from '@modules/public/domain/celcat';

export class CelcatParser {
  private readonly $logger: Logger = new Logger('CelcatParser');

  public fromICS(ics: string): Calendar {
    const lines = ics.split('\r\n');

    const calendar: Calendar = {
      prodid: '',
      version: '',
      x_wr_calname: '',
      x_published_ttl: '',
      events: [],
      extra: [],
    };

    let event: CalendarEvent = {
      uid: '',
      dtstamp: '',
      dtstart: '',
      dtend: '',
      location: '',
      summary: '',
      description: '',
      sequence: '',
      extra: [],
    };

    let isHeader = true,
      isEvent = false,
      previousProperty = '';

    for (const line of lines) {
      switch (true) {
        case isToken(Tokens.BeginCalendar, line):
          isHeader = true;
          break;
        case isToken(Tokens.EndCalendar, line):
          isHeader = false;
          break;
        case isToken(Tokens.BeginEvent, line):
          isEvent = true;
          event = {
            uid: '',
            dtstamp: '',
            dtstart: '',
            dtend: '',
            location: '',
            summary: '',
            description: '',
            sequence: '',
            extra: [],
          };
          break;
        case isToken(Tokens.EndEvent, line):
          isEvent = false;
          calendar.events.push(event);
          break;

        case isToken(Tokens.ProdID, line) && isHeader:
          calendar.prodid = getPropertyValue(line);
          break;
        case isToken(Tokens.Version, line) && isHeader:
          calendar.version = getPropertyValue(line);
          break;
        case isToken(Tokens.CalName, line) && isHeader:
          calendar.x_wr_calname = getPropertyValue(line);
          break;
        case isToken(Tokens.PublishedTTL, line) && isHeader:
          calendar.x_published_ttl = getPropertyValue(line);
          break;
        case isToken(Tokens.Method, line) && isHeader:
          calendar.method = getPropertyValue(line);
          break;

        case isToken(Tokens.UID, line) && isEvent:
          event.uid = getPropertyValue(line);
          previousProperty = 'uid';
          break;
        case isToken(Tokens.DTStamp, line) && isEvent:
          event.dtstamp = getPropertyValue(line);
          previousProperty = 'dtstamp';
          break;
        case isToken(Tokens.DTStart, line) && isEvent:
          event.dtstart = getPropertyValue(line);
          previousProperty = 'dtstart';
          break;
        case isToken(Tokens.DTEnd, line) && isEvent:
          event.dtend = getPropertyValue(line);
          previousProperty = 'dtend';
          break;
        case isToken(Tokens.Location, line) && isEvent:
          event.location = getPropertyValue(line);
          previousProperty = 'location';
          break;
        case isToken(Tokens.Summary, line) && isEvent:
          event.summary = getPropertyValue(line);
          previousProperty = 'summary';
          break;
        case isToken(Tokens.Description, line) && isEvent:
          event.description = getPropertyValue(line);
          previousProperty = 'description';
          break;
        case isToken(Tokens.Sequence, line) && isEvent:
          event.sequence = getPropertyValue(line);
          previousProperty = 'sequence';
          break;

        case line.startsWith(Tokens.Multiline) && isEvent:
          if (previousProperty) {
            event[previousProperty] += unescapeValue(line.substring(1));
          }
          break;

        default:
          this.$logger.warn(`Unknown token: ${line}`);
          if (isEvent) {
            event.extra.push(line);
          } else {
            calendar.extra.push(line);
          }
          break;
      }
    }

    return calendar;
  }

  public toICS(calendar: Calendar, timezone: CalendarTimezone): string {
    const lines: string[] = [];

    lines.push(Tokens.BeginCalendar);
    lines.push(`${Tokens.ProdID}:${calendar.prodid}`);
    lines.push(`${Tokens.Version}:${calendar.version}`);
    lines.push(`${Tokens.CalName}:${escapeValue(calendar.x_wr_calname)}`);
    lines.push(`${Tokens.PublishedTTL}:${calendar.x_published_ttl}`);
    lines.push(`${Tokens.Timezone}:${timezone}`);

    if (calendar.method) {
      lines.push(`${Tokens.Method}:${calendar.method}`);
    }

    calendar.extra.forEach((line) => lines.push(line));

    for (const event of calendar.events) {
      lines.push(Tokens.BeginEvent);
      lines.push(`${Tokens.UID}:${event.uid}`);
      lines.push(`${Tokens.DTStamp}:${event.dtstamp}`);

      if (event.dtstart.startsWith(';')) {
        lines.push(`${Tokens.DTStart}${event.dtstart}`);
      } else {
        lines.push(`${Tokens.DTStart}:${event.dtstart}`);
      }

      if (event.dtend.startsWith(';')) {
        lines.push(`${Tokens.DTEnd}${event.dtend}`);
      } else {
        lines.push(`${Tokens.DTEnd}:${event.dtend}`);
      }

      lines.push(`${Tokens.Location}:${event.location}`);
      lines.push(`${Tokens.Summary}:${escapeValue(event.summary)}`);
      lines.push(`${Tokens.Description}:${escapeValue(event.description)}`);
      lines.push(`${Tokens.Sequence}:${event.sequence}`);
      event.extra.forEach((line) => lines.push(line));

      if (event.color) {
        lines.push(`${Tokens.Color}:${event.color}`);
      }

      if (event.category) {
        lines.push(`${Tokens.Categories}:${event.category}`);
      }

      lines.push(Tokens.EndEvent);
    }

    lines.push(Tokens.EndCalendar);

    return lines.join('\r\n');
  }

  public parseICSDate(date: string, useTime?: boolean): Date {
    const year = parseInt(date.substring(0, 4)),
      month = parseInt(date.substring(4, 6)),
      day = parseInt(date.substring(6, 8));

    if (useTime) {
      const hour = parseInt(date.substring(9, 11)),
        minute = parseInt(date.substring(11, 13)),
        second = parseInt(date.substring(13, 15));

      return new Date(year, month, day, hour, minute, second);
    } else {
      return new Date(year, month, day);
    }
  }

  public parseEventArguments(event: CalendarEvent): {
    [key: string]: string;
  } {
    const argv: { [key: string]: string } = {},
      args: string[] = event.description
        .split('\n')
        .flatMap((line: string) => line.split('; '))
        .filter((arg: string) => arg);

    let lastArg = '';

    args.forEach((arg) => {
      if (!arg.includes(Tokens.DescriptionDelimiter) && lastArg) {
        argv[lastArg] += `, ${arg}`;
        return;
      }

      const argRaw = arg.split(Tokens.DescriptionDelimiter),
        key = argRaw[0];

      argv[key] = argRaw.slice(1).join(Tokens.DescriptionDelimiter);
      lastArg = key;
    });

    return argv;
  }
}

function isToken(token: string, line: string): boolean {
  return line.startsWith(token);
}
function getPropertyValue(line: string): string {
  const value = line.split(Tokens.Colon).slice(1).join(Tokens.Colon);

  return unescapeValue(value);
}
function unescapeValue(value: string): string {
  return EscapedTokens.reduce(
    (acc, [escaped, replacement]) => acc.split(escaped).join(replacement),
    value
  );
}
function escapeValue(value: string): string {
  return EscapedTokens.reduce(
    (acc, [escaped, replacement]) => acc.split(replacement).join(escaped),
    value
  );
}

import axios from 'axios';
import { Injectable } from '@nestjs/common';

import { CelcatParser } from './celcat.parser';
import { CelcatFormatter } from './celcat.formatter';
import { CelcatQuery } from './celcat.query-handler';

@Injectable()
export class CelcatService {
  private readonly $parser: CelcatParser = new CelcatParser();
  private readonly $formatter: CelcatFormatter;
  constructor() {
    this.$formatter = new CelcatFormatter(this.$parser);
  }

  async getCalendar(query: CelcatQuery): Promise<string> {
    const ics = await this._fetchICS(query.config.id);
    const calendar = this.$parser.fromICS(ics);

    const formattedCalendar = this.$formatter.formatCalendar(calendar, {
      type: query.config.type,
      ...query.typeConfig,
    });

    return this.$parser.toICS(formattedCalendar, query.typeConfig.timezone);
  }

  private async _fetchICS(id: string): Promise<string> {
    const response = await axios.get(
      `https://celcat.u-bordeaux.fr/ICalFeed/ical/${id}/schedule.ics`
    );

    return response.data;
  }
}

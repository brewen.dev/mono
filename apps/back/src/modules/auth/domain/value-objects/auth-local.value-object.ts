import { ValueObject } from '@libs/ddd';
import {
  ArgumentInvalidException,
  ArgumentNotProvidedException,
} from '@libs/exceptions';
import { Guard } from '@libs/guard';

export interface AuthLocalProps {
  password: string;
}

export class AuthLocal extends ValueObject<AuthLocalProps> {
  get password(): string {
    return this.props.password;
  }

  protected validate(props: AuthLocalProps): void {
    if (Guard.isEmpty(props.password)) {
      throw new ArgumentNotProvidedException('Password is required');
    }

    if (!Guard.isHash(props.password)) {
      throw new ArgumentInvalidException('Password must be hashed');
    }
  }
}

import { ValueObject } from '@libs/ddd';
import { ArgumentNotProvidedException } from '@libs/exceptions';

export interface AuthGoogleProps {
  id: string;
}

export class AuthGoogle extends ValueObject<AuthGoogleProps> {
  get id(): string {
    return this.props.id;
  }

  protected validate(props: AuthGoogleProps): void {
    if (!props.id) {
      throw new ArgumentNotProvidedException('Id is required');
    }
  }
}

import { AuthType } from '@modules/auth/domain/auth.types';
import { ValueObject } from '@libs/ddd';
import { ArgumentNotProvidedException } from '@libs/exceptions';

import { AuthLocal } from './auth-local.value-object';
import { AuthGoogle } from './auth-google.value-object';

export interface AuthProps {
  type: AuthType;
  local?: AuthLocal;
  google?: AuthGoogle;
}

export class Auth extends ValueObject<AuthProps> {
  get type(): AuthType {
    return this.props.type;
  }

  get local(): AuthLocal | null {
    return this.props.local ?? null;
  }

  get google(): AuthGoogle | null {
    return this.props.google ?? null;
  }

  protected validate(props: AuthProps): void {
    if (!props.type) {
      throw new ArgumentNotProvidedException('Auth type is required');
    }

    if (props.type === AuthType.Local && !props.local) {
      throw new ArgumentNotProvidedException(
        'Auth local is required as type is local'
      );
    }

    if (props.type === AuthType.Google && !props.google) {
      throw new ArgumentNotProvidedException(
        'Auth google is required as type is google'
      );
    }
  }
}

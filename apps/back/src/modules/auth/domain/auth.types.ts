export enum AuthType {
  Local = 'local',
  Google = 'google',
}

export interface CreateUserProps {
  username: string;
  email: string;
  password: string;
}

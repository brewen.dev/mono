import { Injectable } from '@nestjs/common';
import { PassportSerializer } from '@nestjs/passport';

import { UserEntity } from '@modules/user/domain/user.entity';

import { AuthMapper } from '../../auth.mapper';

@Injectable()
export class SessionSerializer extends PassportSerializer {
  constructor(private readonly mapper: AuthMapper) {
    super();
  }

  serializeUser(user: UserEntity, done: CallableFunction): void {
    user.deleteProp('auth');
    done(null, this.mapper.toPersistence(user));
  }
  deserializeUser(payload: any, done: CallableFunction): void {
    done(null, this.mapper.toDomain(payload));
  }
}

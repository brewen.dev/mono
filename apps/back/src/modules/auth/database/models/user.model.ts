import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

import { DefaultSchema } from '@libs/db/schema.base';
import { MongoCollections } from '@configs/mongo.collections';
import { IUser, User } from '@modules/user/database/models/user.model';

import { IUserAuth, UserAuth } from './auth.model';

@Schema({
  ...DefaultSchema,
  collection: MongoCollections.Users,
})
export class AuthUser extends User {
  @Prop({
    type: UserAuth,
    required: true,
  })
  auth: UserAuth;
}

export type AuthUserDocument = AuthUser & Document;
export const AuthUserSchema = SchemaFactory.createForClass(AuthUser);

export interface IAuthUser extends IUser {
  auth: IUserAuth;
}

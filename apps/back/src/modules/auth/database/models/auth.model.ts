import { Prop, Schema } from '@nestjs/mongoose';
import { CleanupSchema } from '@libs/db/schema.base';
import { AuthType } from '../../domain/auth.types';

import { ILocalAuth, LocalAuth } from './local-auth.model';
import { GoogleAuth, IGoogleAuth } from './google-auth.model';

@Schema(CleanupSchema)
export class UserAuth implements IUserAuth {
  @Prop({
    type: String,
    required: true,
    validate: {
      validator: (v: AuthType) => Object.values(AuthType).includes(v),
    },
  })
  type: AuthType;

  @Prop({
    type: LocalAuth,
    required: () => (this as any)?.type === AuthType.Local,
  })
  local?: LocalAuth;

  @Prop({
    type: GoogleAuth,
    required: () => (this as any)?.type === AuthType.Google,
  })
  google?: GoogleAuth;
}

export interface IUserAuth {
  type: AuthType;
  local?: ILocalAuth;
  google?: IGoogleAuth;
}

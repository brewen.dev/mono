import { Prop, Schema } from '@nestjs/mongoose';
import { CleanupSchema } from '@libs/db/schema.base';

@Schema(CleanupSchema)
export class GoogleAuth implements IGoogleAuth {
  @Prop({
    type: String,
    required: true,
    unique: true,
    index: true,
    sparse: true,
  })
  id: string;
}

export interface IGoogleAuth {
  id: string;
}

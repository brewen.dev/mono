import { Prop, Schema } from '@nestjs/mongoose';
import { CleanupSchema } from '@libs/db/schema.base';

@Schema(CleanupSchema)
export class LocalAuth implements ILocalAuth {
  @Prop({
    type: String,
    required: true,
  })
  password: string;
}

export interface ILocalAuth {
  password: string;
}

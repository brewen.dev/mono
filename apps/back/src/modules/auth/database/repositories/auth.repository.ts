import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Model } from 'mongoose';

import { MongoRepositoryBase } from '@libs/db/mongo-repository.base';

import { UserEntity } from '@modules/user/domain/user.entity';

import { AuthUser, IAuthUser } from '@modules/auth/database/models/user.model';
import { AuthMapper } from '../../auth.mapper';
import { AuthRepositoryPort } from './auth.repository.port';

@Injectable()
export class AuthRepository
  extends MongoRepositoryBase<UserEntity, IAuthUser>
  implements AuthRepositoryPort
{
  constructor(
    @InjectModel(AuthUser.name) protected readonly model: Model<AuthUser>,
    mapper: AuthMapper,
    eventEmitter: EventEmitter2
  ) {
    super(mapper, eventEmitter, new Logger(AuthRepository.name));
  }

  async findOneByEmail(email: string): Promise<UserEntity | null> {
    const user = await this.model.findOne({ email }).exec();

    if (!user) {
      return null;
    }

    return this.mapper.toDomain(user);
  }
}

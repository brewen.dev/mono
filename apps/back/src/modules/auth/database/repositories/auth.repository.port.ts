import { RepositoryPort } from '@libs/ddd';
import { UserEntity } from '@modules/user/domain/user.entity';

export interface AuthRepositoryPort extends RepositoryPort<UserEntity> {
  findOneByEmail(email: string): Promise<UserEntity | null>;
}

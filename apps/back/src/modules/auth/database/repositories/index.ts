import { Provider } from '@nestjs/common';

import { AUTH_REPOSITORY } from '../../auth.tokens';
import { AuthRepository } from './auth.repository';

export const Repositories: Provider[] = [
  {
    provide: AUTH_REPOSITORY,
    useClass: AuthRepository,
  },
];

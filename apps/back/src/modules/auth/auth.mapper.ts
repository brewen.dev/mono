import { Injectable } from '@nestjs/common';

import { Mapper } from '@libs/ddd';
import { MongoUtils } from '@libs/utils/mongo.util';

import { UserEntity } from '@modules/user/domain/user.entity';
import { UserResponseDto } from '@modules/user/dto/user.response.dto';

import { IAuthUser } from '@modules/auth/database/models/user.model';
import { AuthType } from './domain/auth.types';

import { Auth } from './domain/value-objects/auth.value-object';
import { AuthLocal } from './domain/value-objects/auth-local.value-object';
import { AuthGoogle } from './domain/value-objects/auth-google.value-object';

@Injectable()
export class AuthMapper
  implements Mapper<UserEntity, IAuthUser, UserResponseDto>
{
  toPersistence(entity: UserEntity): IAuthUser {
    const copy = entity.getProps();

    return {
      _id: MongoUtils.toObjectId(copy.id),
      createdAt: copy.createdAt,
      updatedAt: copy.updatedAt,

      username: copy.username,
      displayName: copy.displayName,
      email: copy.email,
      roles: copy.roles,
      auth: copy.auth
        ? {
            type: copy.auth.type,
            local:
              copy.auth.type === AuthType.Local && copy.auth.local
                ? {
                    password: copy.auth.local.password,
                  }
                : null,
            google:
              copy.auth.type === AuthType.Google && copy.auth.google
                ? {
                    id: copy.auth.google?.id,
                  }
                : null,
          }
        : null,
    };
  }

  toDomain(record: IAuthUser): UserEntity {
    return new UserEntity({
      id: MongoUtils.fromObjectId(record._id),
      createdAt: record.createdAt,
      updatedAt: record.updatedAt,
      props: {
        username: record.username,
        displayName: record.displayName,
        email: record.email,
        roles: record.roles,
        auth: record.auth
          ? new Auth({
              type: record.auth.type,
              local:
                record.auth.type === AuthType.Local
                  ? new AuthLocal(record.auth.local)
                  : null,
              google:
                record.auth.type === AuthType.Google
                  ? new AuthGoogle(record.auth.google)
                  : null,
            })
          : null,
      },
    });
  }

  toResponse(entity: UserEntity): UserResponseDto {
    const props = entity.getProps();
    const response = new UserResponseDto(entity);
    response.id = props.id;
    response.username = props.username;
    response.displayName = props.displayName;
    response.email = props.email;
    response.roles = props.roles;
    return response;
  }
}

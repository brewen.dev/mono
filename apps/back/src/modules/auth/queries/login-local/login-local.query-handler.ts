import { Inject } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { Err, Ok, Result } from 'oxide.ts';

import { Params, QueryBase } from '@libs/ddd/query.base';

import { UserEntity } from '@modules/user/domain/user.entity';

import { AUTH_REPOSITORY } from '../../auth.tokens';
import { AuthRepository } from '@modules/auth/database/repositories/auth.repository';
import { InvalidCredentialsError } from '../../domain/auth.errors';

export class LoginUserQuery extends QueryBase {
  readonly email: string;
  readonly password: string;

  constructor(props: Params<LoginUserQuery>) {
    super();
    this.email = props.email;
    this.password = props.password;
  }
}

@QueryHandler(LoginUserQuery)
export class LoginLocalQueryHandler implements IQueryHandler {
  constructor(
    @Inject(AUTH_REPOSITORY) private readonly repository: AuthRepository
  ) {}

  async execute(query: LoginUserQuery): Promise<Result<UserEntity, Error>> {
    const user = await this.repository.findOneByEmail(query.email);

    if (!user || !user.comparePassword(query.password)) {
      return Err(new InvalidCredentialsError());
    }

    return Ok(user);
  }
}

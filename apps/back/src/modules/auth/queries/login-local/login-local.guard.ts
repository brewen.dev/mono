import {
  BadRequestException,
  ExecutionContext,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';

import { ApiErrorResponse } from '@libs/api/api-error.response';
import { RequestContextService } from '@libs/application/context/AppRequestContext';

import { LoginLocalRequestDto } from './login-local.request.dto';

@Injectable()
export class LocalAuthGuard extends AuthGuard('local') {
  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();

    const body = plainToInstance(LoginLocalRequestDto, request.body);
    const errors = await validate(body);
    const errorMessages: string[] = errors.flatMap(({ constraints }: any) =>
      Object.values(constraints)
    );

    if (errorMessages.length > 0) {
      throw new BadRequestException(
        new ApiErrorResponse({
          statusCode: HttpStatus.BAD_REQUEST,
          message: 'Validation error',
          error: 'Bad Request',
          subErrors: errorMessages,
          correlationId: RequestContextService.getRequestId(),
        })
      );
    }

    const result = (await super.canActivate(context)) as boolean;

    await super.logIn(request);
    return result;
  }
}

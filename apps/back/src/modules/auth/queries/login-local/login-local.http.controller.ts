import {
  Body,
  Controller,
  Post,
  UseGuards,
  Req,
  HttpStatus,
  HttpCode,
} from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';

import { RoutesV1 } from '@configs/app.routes';
import { SwaggerTags } from '@configs/app.config';
import { ApiErrorResponse } from '@libs/api/api-error.response';
import { UserResponseDto } from '@modules/user/dto/user.response.dto';

import { LoginLocalRequestDto } from './login-local.request.dto';
import { LocalAuthGuard } from './login-local.guard';
import { AuthMapper } from '../../auth.mapper';
import { UserEntity } from '@modules/user/domain/user.entity';

@ApiTags(SwaggerTags.Authentication)
@Controller(RoutesV1.version)
export class LoginLocalHttpController {
  constructor(private readonly mapper: AuthMapper) {}

  @ApiOperation({ summary: 'Login with local strategy (email & password)' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'User logged in successfully',
    type: UserResponseDto,
    headers: {
      'Set-Cookie': {
        description:
          'Cookie with session id (connect.sid) that expires in 7 days',
        schema: {
          type: 'string',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Invalid credentials',
    type: ApiErrorResponse,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ApiErrorResponse,
  })
  @UseGuards(LocalAuthGuard)
  @HttpCode(HttpStatus.OK)
  @Post([RoutesV1.auth.post.login.default, RoutesV1.auth.post.login.local])
  async login(
    @Body() props: LoginLocalRequestDto,
    @Req() request: Request
  ): Promise<UserResponseDto> {
    return this.mapper.toResponse(request.user as UserEntity);
  }
}

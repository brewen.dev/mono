import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString, MaxLength, MinLength } from 'class-validator';
import { ValidationOptions } from '@configs/app.const';

export class LoginLocalRequestDto {
  @ApiProperty({
    example: 'john.doe@example.com',
    description: 'The email of the user',
    pattern: ValidationOptions.email.match.toString(),
  })
  @MinLength(ValidationOptions.email.minLength)
  @MaxLength(ValidationOptions.email.maxLength)
  @IsString()
  @IsEmail()
  readonly email: string;

  @ApiProperty({
    example: 'password',
    description: 'The password of the user',
    minLength: ValidationOptions.password.minLength,
    maxLength: ValidationOptions.password.maxLength,
  })
  @IsString()
  @MinLength(ValidationOptions.password.minLength)
  @MaxLength(ValidationOptions.password.maxLength)
  readonly password: string;
}

import {
  Injectable,
  ConflictException as ConflictHttpException,
} from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { QueryBus } from '@nestjs/cqrs';
import { Strategy } from 'passport-local';
import { match, Result } from 'oxide.ts';

import { UserEntity } from '@modules/user/domain/user.entity';

import { LoginUserQuery } from './login-local.query-handler';
import { InvalidCredentialsError } from '../../domain/auth.errors';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(private readonly queryBus: QueryBus) {
    super({
      usernameField: 'email',
      passwordField: 'password',
    });
  }

  async validate(email: string, password: string): Promise<UserEntity> {
    const query = new LoginUserQuery({ email, password });

    const result: Result<UserEntity, Error> = await this.queryBus.execute(
      query
    );

    return match(result, {
      Ok: (user: UserEntity) => user,
      Err: (error: Error): any => {
        if (error instanceof InvalidCredentialsError)
          throw new ConflictHttpException(error.message);
        throw error;
      },
    });
  }
}

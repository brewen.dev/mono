import {
  Controller,
  Delete,
  HttpStatus,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';

import { SwaggerTags } from '@configs/app.config';
import { RoutesV1 } from '@configs/app.routes';
import { SessionGuard } from '@libs/guards/session.guard';
import { ApiErrorResponse } from '@libs/api/api-error.response';

@ApiTags(SwaggerTags.Authentication)
@Controller(RoutesV1.version)
export class LogoutHttpController {
  @ApiOperation({ summary: 'Logout of the current session' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Successfully logged out of the current session',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Session not found or already expired',
    type: ApiErrorResponse,
  })
  @UseGuards(SessionGuard)
  @Delete(RoutesV1.auth.delete.logout)
  async logout(
    @Req() request: Request,
    @Res() response: Response
  ): Promise<void> {
    request.session.destroy(() => {
      response.clearCookie('connect.sid');
      response.status(HttpStatus.NO_CONTENT).send();
    });
  }
}

import { Command, CommandProps } from '@libs/ddd';

export class RegisterUserCommand extends Command {
  readonly username: string;
  readonly email: string;
  readonly password: string;

  constructor(props: CommandProps<RegisterUserCommand>) {
    super(props);
    this.username = props.username;
    this.email = props.email;
    this.password = props.password;
  }
}

import { CommandBus } from '@nestjs/cqrs';
import { Inject, Logger } from '@nestjs/common';
import { Console, Command } from 'nestjs-console';

import { LoggerPort } from '@libs/ports/logger.port';

import { RegisterUserCommand } from './register-user.command';

@Console({
  command: 'register',
  description: 'A command to register a new user',
})
export class RegisterUserCliController {
  constructor(
    private readonly commandBus: CommandBus,
    @Inject(Logger)
    private readonly logger: LoggerPort
  ) {}

  @Command({
    command: 'user <username> <email> <password>',
    description: 'Register a new user',
  })
  async registerUser(
    username: string,
    email: string,
    password: string
  ): Promise<void> {
    const command = new RegisterUserCommand({
      username,
      email,
      password,
    });

    const result = await this.commandBus.execute(command);

    this.logger.log(
      `User ${result.username} registered successfully`,
      result.unwrap()
    );
  }
}

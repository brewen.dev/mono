import {
  Body,
  ConflictException as ConflictHttpException,
  Controller,
  HttpStatus,
  Post,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { match, Result } from 'oxide.ts';

import { RoutesV1 } from '@configs/app.routes';
import { SwaggerTags } from '@configs/app.config';

import { IdResponse } from '@libs/api/id.response.dto';
import { ApiErrorResponse } from '@libs/api/api-error.response';
import { AggregateID } from '@libs/ddd';

import { UserAlreadyExistsError } from '../../domain/auth.errors';
import { RegisterUserRequestDto } from './register-user.request.dto';
import { RegisterUserCommand } from './register-user.command';

@ApiTags(SwaggerTags.Authentication)
@Controller(RoutesV1.version)
export class RegisterUserHttpController {
  constructor(private readonly commandBus: CommandBus) {}

  @ApiOperation({ summary: 'Register a new user' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: IdResponse,
  })
  @ApiResponse({
    status: HttpStatus.CONFLICT,
    description: UserAlreadyExistsError.message,
    type: ApiErrorResponse,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ApiErrorResponse,
  })
  @Post([
    RoutesV1.auth.post.register.default,
    RoutesV1.auth.post.register.local,
  ])
  async register(@Body() body: RegisterUserRequestDto): Promise<IdResponse> {
    const command = new RegisterUserCommand(body);

    const result: Result<AggregateID, UserAlreadyExistsError> =
      await this.commandBus.execute(command);

    return match(result, {
      Ok: (id: AggregateID) => new IdResponse(id),
      Err: (error: Error): any => {
        if (error instanceof UserAlreadyExistsError)
          throw new ConflictHttpException(error.message);
        throw error;
      },
    });
  }
}

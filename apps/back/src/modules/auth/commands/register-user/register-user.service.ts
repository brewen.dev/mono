import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Inject } from '@nestjs/common';
import { Err, Ok, Result } from 'oxide.ts';

import { AggregateID } from '@libs/ddd';
import { ConflictException } from '@libs/exceptions';
import { Encrypt } from '@libs/utils/encrypt';

import { UserEntity } from '@modules/user/domain/user.entity';

import { RegisterUserCommand } from './register-user.command';
import { AUTH_REPOSITORY } from '../../auth.tokens';
import { AuthRepositoryPort } from '@modules/auth/database/repositories/auth.repository.port';
import { AuthType } from '../../domain/auth.types';
import { Auth } from '../../domain/value-objects/auth.value-object';
import { UserAlreadyExistsError } from '../../domain/auth.errors';
import { AuthLocal } from '../../domain/value-objects/auth-local.value-object';

@CommandHandler(RegisterUserCommand)
export class RegisterUserService implements ICommandHandler {
  constructor(
    @Inject(AUTH_REPOSITORY)
    protected readonly authRepository: AuthRepositoryPort
  ) {}

  async execute(
    command: RegisterUserCommand
  ): Promise<Result<AggregateID, UserAlreadyExistsError>> {
    const hashedPassword = Encrypt.hash(command.password);

    const user = UserEntity.create({
      username: command.username,
      email: command.email,
      auth: new Auth({
        type: AuthType.Local,
        local: new AuthLocal({
          password: hashedPassword,
        }),
      }),
    });

    try {
      await this.authRepository.transaction(async () =>
        this.authRepository.insert(user)
      );
      return Ok(user.id);
    } catch (error: any) {
      if (error instanceof ConflictException) {
        return Err(new UserAlreadyExistsError(error));
      }
      throw error;
    }
  }
}

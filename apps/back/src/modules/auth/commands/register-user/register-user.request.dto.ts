import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';
import { ValidationOptions } from '@configs/app.const';

export class RegisterUserRequestDto {
  @ApiProperty({
    example: 'john.doe@example.com',
    description: 'The email of the user',
    pattern: ValidationOptions.email.match.toString(),
    minLength: ValidationOptions.email.minLength,
    maxLength: ValidationOptions.email.maxLength,
  })
  @MaxLength(ValidationOptions.email.maxLength)
  @MinLength(ValidationOptions.email.minLength)
  @IsEmail()
  readonly email: string;

  @ApiProperty({
    example: 'john.doe',
    description: 'The username of the user',
    pattern: ValidationOptions.username.match.toString(),
    minLength: ValidationOptions.username.minLength,
    maxLength: ValidationOptions.username.maxLength,
  })
  @MaxLength(ValidationOptions.username.maxLength)
  @MinLength(ValidationOptions.username.minLength)
  @IsString()
  @Matches(ValidationOptions.username.match, {
    message:
      'The username must contain only letters, numbers, underscores and dots',
  })
  readonly username: string;

  @ApiProperty({
    example: 'passw0rd',
    description: 'The password of the user',
    minLength: ValidationOptions.password.minLength,
    maxLength: ValidationOptions.password.maxLength,
  })
  @MaxLength(ValidationOptions.password.maxLength)
  @MinLength(ValidationOptions.password.minLength)
  @IsString()
  readonly password: string;
}

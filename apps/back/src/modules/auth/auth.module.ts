import { Logger, Module, Provider } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CqrsModule } from '@nestjs/cqrs';
import { PassportModule } from '@nestjs/passport';

import {
  AuthUser,
  AuthUserSchema,
} from '@modules/auth/database/models/user.model';
import { Repositories } from './database/repositories';

import { RegisterUserHttpController } from './commands/register-user/register-user.http.controller';
import { RegisterUserCliController } from './commands/register-user/register-user.cli.controller';
import { RegisterUserService } from './commands/register-user/register-user.service';

import { LogoutHttpController } from './commands/logout/logout.http.controller';

import { LocalStrategy } from './queries/login-local/login-local.strategy';
import { LoginLocalHttpController } from './queries/login-local/login-local.http.controller';
import { LoginLocalQueryHandler } from './queries/login-local/login-local.query-handler';

import { AuthMapper } from './auth.mapper';
import { SessionSerializer } from './domain/serializers/session.serializer';

const HttpControllers = [
  RegisterUserHttpController,
  LoginLocalHttpController,
  LogoutHttpController,
];
const CliControllers: Provider[] = [RegisterUserCliController];

const CommandHandlers: Provider[] = [RegisterUserService];
const QueryHandlers: Provider[] = [LoginLocalQueryHandler];

const Mappers: Provider[] = [AuthMapper];
const Strategies: Provider[] = [LocalStrategy];
const Serializers: Provider[] = [SessionSerializer];

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: AuthUser.name,
        schema: AuthUserSchema,
      },
    ]),

    PassportModule.register({
      session: true,
      defaultStrategy: 'local',
    }),

    CqrsModule,
  ],
  controllers: [...HttpControllers],
  providers: [
    Logger,
    ...CliControllers,
    ...Repositories,
    ...CommandHandlers,
    ...QueryHandlers,
    ...Mappers,
    ...Strategies,
    ...Serializers,
  ],
})
export class AuthModule {}

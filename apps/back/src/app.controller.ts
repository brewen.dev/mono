import { Controller, Get, Redirect } from '@nestjs/common';

import { DOMAIN } from '@configs/app.config';

@Controller()
export class AppController {
  @Get()
  @Redirect(`https://${DOMAIN}`)
  public async index(): Promise<void> {}
}

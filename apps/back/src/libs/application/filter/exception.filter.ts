import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import * as Sentry from '@sentry/node';
import { nanoid } from 'nanoid';

import { isProduction } from '@configs/app.config';

import { RequestContextService } from '../context/AppRequestContext';
import { ApiErrorResponse } from '../../api/api-error.response';

@Catch()
export class GlobalExceptionFilter implements ExceptionFilter {
  private readonly logger = new Logger(GlobalExceptionFilter.name);

  catch(exception: any, host: ArgumentsHost): void {
    if (!exception.correlationId) {
      if (!RequestContextService.getRequestId()) {
        RequestContextService.setRequestId(nanoid(6));
      }

      exception.correlationId = RequestContextService.getRequestId();
    }

    if (exception.response) {
      exception.response.correlationId = exception.correlationId;
    }

    if (!(exception instanceof HttpException)) {
      this.logger.error(
        `[${RequestContextService.getRequestId()}] ${exception.message}`
      );

      if (isProduction()) {
        Sentry.captureException(exception);
      }
    }

    if (exception.status >= 400 && exception.status < 500) {
      this.logger.debug(
        `[${RequestContextService.getRequestId()}] ${exception.message}`
      );
    }

    const ctx = host.switchToHttp();
    const response = ctx.getResponse();

    const status =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;
    const error: string =
      exception instanceof HttpException
        ? typeof exception.getResponse() === 'string'
          ? exception.getResponse()
          : exception.getResponse()['error'] ??
            exception.getResponse()['message']
        : 'Internal server error';

    response.status(status).json(
      new ApiErrorResponse({
        statusCode: status,
        message: exception.message,
        error: error,
        correlationId: exception.correlationId,
        subErrors: exception?.subErrors ?? exception?.response?.subErrors,
      })
    );
  }
}

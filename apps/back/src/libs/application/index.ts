import { Provider } from '@nestjs/common';
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';

import { ContextInterceptor } from './context/ContextInterceptor';
import { ExceptionInterceptor } from './interceptors/exception.interceptor';

import { GlobalExceptionFilter } from './filter/exception.filter';

export const Interceptors: Provider[] = [
  {
    provide: APP_INTERCEPTOR,
    useClass: ContextInterceptor,
  },
  {
    provide: APP_INTERCEPTOR,
    useClass: ExceptionInterceptor,
  },
];

export const ExceptionFilters: Provider[] = [
  {
    provide: APP_FILTER,
    useClass: GlobalExceptionFilter,
  },
];

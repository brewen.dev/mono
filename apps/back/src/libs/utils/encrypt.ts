import { hashSync, compareSync } from 'bcrypt';

export class Encrypt {
  static hash(plaintext: string): string {
    return hashSync(plaintext, 10);
  }

  static hashWithSalt(plaintext: string, salt: string): string {
    return hashSync(plaintext, salt);
  }

  static compare(plaintext: string, hash: string): boolean {
    return compareSync(plaintext, hash);
  }
}

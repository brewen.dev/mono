import { ExecutionContext, Injectable } from '@nestjs/common';
import { BaseGuard } from '@libs/api/guard.base';
import { NotFoundException as NotFoundHttpException } from '@nestjs/common/exceptions/not-found.exception';

@Injectable()
export class SessionGuard extends BaseGuard {
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    if (!request.isAuthenticated()) {
      throw new NotFoundHttpException('Session not found or already expired');
    }

    return true;
  }
}

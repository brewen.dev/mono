import {
  ExecutionContext,
  ForbiddenException as ForbiddenHttpException,
  Injectable,
} from '@nestjs/common';
import { InvalidSessionException } from '@libs/exceptions';
import { BaseGuard } from '@libs/api/guard.base';

@Injectable()
export class AuthenticatedGuard extends BaseGuard {
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    if (!request.isAuthenticated()) {
      throw new ForbiddenHttpException(InvalidSessionException.message);
    }

    return true;
  }
}

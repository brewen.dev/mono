import { CanActivate, ExecutionContext, Logger } from '@nestjs/common';
import { Request, Response } from 'express';

import { ObjectLiteral } from '../types';

export abstract class BaseGuard implements CanActivate {
  protected readonly logger: Logger = new Logger(BaseGuard.name);

  abstract canActivate(context: ExecutionContext): Promise<boolean>;

  protected getRequest(context: ExecutionContext): Request {
    return context.switchToHttp().getRequest();
  }
  protected getResponse(context: ExecutionContext): Response {
    return context.switchToHttp().getResponse();
  }

  protected getBody(context: ExecutionContext): any {
    return this.getRequest(context).body;
  }
  protected getParams(context: ExecutionContext): ObjectLiteral {
    return this.getRequest(context).params;
  }
  protected getQuery(context: ExecutionContext): ObjectLiteral {
    return this.getRequest(context).query;
  }
}

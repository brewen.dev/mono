export const DefaultSchema = {
  versionKey: false,
  timestamps: true,
};

export const CleanupSchema = {
  timestamps: false,
  versionKey: false,
  id: false,
};

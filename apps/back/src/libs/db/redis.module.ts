import { DynamicModule, Module } from '@nestjs/common';
import Redis, { RedisOptions } from 'ioredis';

export const REDIS = Symbol('DB.REDIS');

@Module({})
export class RedisModule {
  static forRoot(uri: string): DynamicModule {
    return {
      module: RedisModule,
      providers: [
        {
          provide: REDIS,
          useValue: new Redis(uri),
        },
      ],
      exports: [REDIS],
    };
  }

  static forRootAsync(options: {
    imports: any[];
    useFactory: (...args: any[]) => Promise<RedisOptions | string>;
    inject: any[];
  }): DynamicModule {
    return {
      module: RedisModule,
      imports: options.imports,
      providers: [
        {
          provide: REDIS,
          useFactory: async (...args: any[]): Promise<Redis> => {
            const redisOptions: any = await options.useFactory(...args);
            return new Redis(redisOptions);
          },
          inject: options.inject,
        },
      ],
      exports: [REDIS],
    };
  }
}

import {
  Inject,
  Logger,
  MiddlewareConsumer,
  Module,
  NestModule,
} from '@nestjs/common';

import { RequestContextModule } from 'nestjs-request-context';
import Redis from 'ioredis';
import RedisStore from 'connect-redis';
import session from 'express-session';
import passport from 'passport';
import { ConsoleModule } from 'nestjs-console';

import { EventEmitterModule } from '@nestjs/event-emitter';
import { ConfigModule } from '@nestjs/config';
import { ThrottlerModule } from '@nestjs/throttler';
import { MongooseModule } from '@nestjs/mongoose';
import { CqrsModule } from '@nestjs/cqrs';

import { REDIS, RedisModule } from '@libs/db/redis.module';
import { ExceptionFilters, Interceptors } from '@libs/application';

import { MongoUri } from '@configs/mongo.config';
import { RedisUri } from '@configs/redis.config';
import { ThrottleConfig } from '@configs/throttle.config';
import { SessionConfig } from '@configs/session.config';

import { AuthModule } from '@modules/auth/auth.module';
import { PublicModule } from '@modules/public/public.module';

import { AppController } from './app.controller';
import { UserModule } from '@modules/user/user.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    EventEmitterModule.forRoot(),
    RequestContextModule,
    CqrsModule,
    ConsoleModule,

    ThrottlerModule.forRoot(ThrottleConfig),
    MongooseModule.forRoot(MongoUri),
    RedisModule.forRoot(RedisUri),

    AuthModule,
    UserModule,
    PublicModule,
  ],
  controllers: [AppController],
  providers: [...Interceptors, ...ExceptionFilters],
})
export class AppModule implements NestModule {
  private readonly $logger: Logger = new Logger('Bootstrap');

  constructor(@Inject(REDIS) private readonly $redis: Redis) {}

  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(
        session({
          ...SessionConfig,
          store: new RedisStore({
            client: this.$redis,
          }),
        }) as any,
        passport.initialize() as any,
        passport.session()
      )
      .exclude('public/(.*)')
      .forRoutes('*');
  }
}

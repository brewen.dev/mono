import { NestFactory } from '@nestjs/core';
import { Logger, ValidationPipe, VersioningType } from '@nestjs/common';
import { SwaggerModule } from '@nestjs/swagger';
import { NestExpressApplication } from '@nestjs/platform-express';

import * as Sentry from '@sentry/node';
import { get } from 'env-var';
import './libs/utils/dotenv';

import { AppModule } from './app.module';

import { isDevelopment, isProduction } from '@configs/app.config';
import { SentryConfig } from '@configs/sentry.config';
import {
  SwaggerConfig,
  SwaggerOptions,
  SwaggerSetup,
} from '@configs/swagger.config';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    logger: isDevelopment()
      ? ['error', 'warn', 'log', 'debug', 'verbose']
      : ['error', 'warn', 'log'],
  });

  app.enableVersioning({
    type: VersioningType.URI,
  });
  app.enableShutdownHooks();
  app.disable('x-powered-by');

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    })
  );

  if (isProduction()) {
    Sentry.init(SentryConfig);
  }

  const SwaggerDocument = SwaggerModule.createDocument(
    app,
    SwaggerConfig,
    SwaggerOptions
  );
  SwaggerModule.setup('docs', app, SwaggerDocument, SwaggerSetup);

  const PORT = get('PORT').default('3000').asPortNumber();
  await app.listen(PORT);

  const logger = new Logger('Bootstrap');
  logger.log(`🚀 Application is running on: http://localhost:${PORT}`);

  process.once('SIGINT', () => app.close());
}
bootstrap();

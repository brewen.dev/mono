import { get } from 'env-var';

const MongoArguments = {
  user: get('MONGO_USER').required().asString(),
  password: get('MONGO_PASSWORD').required().asString(),
  host: get('MONGO_HOST').required().asString(),
  database: get('MONGO_DATABASE').default('dev').asString(),

  isSSL: get('MONGO_SSL').default('false').asBool(),
  options: {
    useNewUrlParser: true,
    w: 'majority',
  },
};

export const MongoUri = `mongodb${MongoArguments.isSSL ? '+srv' : ''}://${
  MongoArguments.user
}:${MongoArguments.password}@${MongoArguments.host}/${
  MongoArguments.database
}?${Object.entries(MongoArguments.options)
  .map(([key, value]) => `${key}=${value}`)
  .join('&')}`;

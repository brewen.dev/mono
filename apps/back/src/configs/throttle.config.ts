import { get } from 'env-var';
import { ThrottlerModuleOptions } from '@nestjs/throttler';

export const ThrottleConfig: ThrottlerModuleOptions = {
  throttlers: [
    {
      ttl: get('THROTTLE_TTL').default('10').asIntPositive(),
      limit: get('THROTTLE_LIMIT').default('60').asIntPositive(),
    },
    {
      name: 'auth',
      ttl: get('THROTTLE_AUTH_TTL').default('30000').asIntPositive(),
      limit: get('THROTTLE_AUTH_LIMIT').default('10').asIntPositive(),
    },
  ],
};

import { DocumentBuilder } from '@nestjs/swagger';
import { DOMAIN, TITLE } from './app.config';

import { AuthModule } from '@modules/auth/auth.module';
import { UserModule } from '@modules/user/user.module';
import { PublicModule } from '@modules/public/public.module';

const SwaggerDescription = `An OpenAPI 3.0 documentation of the API.<br/>Refer to the [GitLab repository](https://gitlab.com/brewen.dev/back) for more information.`;

export const SwaggerConfig = new DocumentBuilder()
  .setTitle(TITLE)
  .setDescription(SwaggerDescription)
  .setVersion('1.0.1')
  .addCookieAuth(
    'connect.sid',
    {
      type: 'apiKey',
      in: 'cookie',
      name: 'connect.sid',
    },
    'session'
  )
  .addServer(`https://api.${DOMAIN}`, 'Production')
  .addServer('http://localhost:3000', 'Development')
  .build();

export const SwaggerOptions = {
  include: [AuthModule, UserModule, PublicModule],
};

export const SwaggerSetup = {
  customSiteTitle: TITLE,
};

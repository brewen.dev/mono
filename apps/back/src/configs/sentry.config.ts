import { get } from 'env-var';
import { NodeOptions } from '@sentry/node';

export const SentryConfig: NodeOptions = {
  dsn: get('SENTRY_DSN').required().asUrlString(),
};

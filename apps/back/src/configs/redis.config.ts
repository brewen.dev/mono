import { get } from 'env-var';

const RedisArguments = {
  user: get('REDIS_USER').default('default').asString(),
  password: get('REDIS_PASSWORD').default('password').asString(),
  host: get('REDIS_HOST').default('localhost').asString(),
  port: get('REDIS_PORT').default('6379').asPortNumber(),

  isTLS: get('REDIS_TLS').default('false').asBool(),
};

export const RedisUri = `redis${RedisArguments.isTLS ? 's' : ''}://${
  RedisArguments.user
}:${RedisArguments.password}@${RedisArguments.host}:${RedisArguments.port}`;

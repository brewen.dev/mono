import { SessionOptions } from 'express-session';
import { get } from 'env-var';

export const SessionConfig: SessionOptions = {
  saveUninitialized: false,
  secret: get('SESSION_SECRET').required().asString(),
  resave: false,
  cookie: {
    sameSite: true,
    httpOnly: false,
    maxAge: 1000 * 60 * 60 * 24 * 7, // 7 days
  },
};

import { AuthType } from '@modules/auth/domain/auth.types';

export enum RoutesRoot {
  Public = '/public',
  Auth = '/auth',
  User = '/user',
}

export enum Versions {
  V1 = 'v1',
  V2 = 'v2',
}

export const RoutesV1 = {
  version: Versions.V1,
  public: {
    celcat: `${RoutesRoot.Public}/celcat/:id/:type?`,
  },
  auth: {
    post: {
      login: {
        default: `${RoutesRoot.Auth}/login`,
        local: `${RoutesRoot.Auth}/login/${AuthType.Local}`,
        google: `${RoutesRoot.Auth}/login/${AuthType.Google}`,
      },

      register: {
        default: `${RoutesRoot.Auth}/register`,
        local: `${RoutesRoot.Auth}/register/${AuthType.Local}`,
        google: `${RoutesRoot.Auth}/register/${AuthType.Google}`,
      },
    },

    delete: {
      logout: `${RoutesRoot.Auth}`,
    },
  },
  user: {
    get: {
      current: `${RoutesRoot.User}`,
      get: `${RoutesRoot.User}s/:id`,
      list: `${RoutesRoot.User}s`,
    },
  },
};

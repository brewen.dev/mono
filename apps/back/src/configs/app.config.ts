import { get } from 'env-var';

export const TITLE = 'Brewen.dev API';
export const DOMAIN = 'brewen.dev';

export function isProduction(): boolean {
  return get('NODE_ENV').required().asString().toLowerCase() === 'production';
}
export function isDevelopment(): boolean {
  return get('NODE_ENV').required().asString().toLowerCase() === 'development';
}

export enum SwaggerTags {
  Authentication = 'Authentication',
  User = 'User',
  Public = 'Public',
}

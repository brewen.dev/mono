const { composePlugins, withNx } = require('@nx/webpack');
const { merge } = require('webpack-merge');

// Nx plugins for webpack.
module.exports = composePlugins(withNx(), (config) => {
  return merge(config, {
    ignoreWarnings: [
      /Failed to parse source map/,
      /Critical dependency: the request of a dependency is an expression/,
    ],
  });
});

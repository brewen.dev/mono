import { NgModule } from '@angular/core';
import {
  BrowserModule,
  HAMMER_GESTURE_CONFIG,
  HammerModule,
} from '@angular/platform-browser';
import { RouterModule, TitleStrategy } from '@angular/router';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { HammerConfig } from '@configs/hammer.config';
import { AppRoutes, RoutesConfig } from '@configs/app.routes';

import { PageTitleStrategy } from '@libs/utils/title.strategy';

import { AppReducers } from '@usecases/state';

import { AppComponent } from './app.component';
import { AdaptersModule } from './adapters';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    StoreModule.forRoot(AppReducers),
    StoreDevtoolsModule.instrument({}),
    RouterModule.forRoot(AppRoutes, RoutesConfig),
    HammerModule,

    AdaptersModule,
  ],
  providers: [
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: HammerConfig,
    },
    {
      provide: TitleStrategy,
      useClass: PageTitleStrategy,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

export const GesturesEvent = {
  Pan: Symbol('GESTURES.PAN'),
  PanStart: Symbol('GESTURES.PAN_START'),
  PanMove: Symbol('GESTURES.PAN_MOVE'),
  PanEnd: Symbol('GESTURES.PAN_END'),
  PanCancel: Symbol('GESTURES.PAN_CANCEL'),
  PanLeft: Symbol('GESTURES.PAN_LEFT'),
  PanRight: Symbol('GESTURES.PAN_RIGHT'),
  PanUp: Symbol('GESTURES.PAN_UP'),
  PanDown: Symbol('GESTURES.PAN_DOWN'),

  Pinch: Symbol('GESTURES.PINCH'),
  PinchStart: Symbol('GESTURES.PINCH_START'),
  PinchMove: Symbol('GESTURES.PINCH_MOVE'),
  PinchEnd: Symbol('GESTURES.PINCH_END'),
  PinchCancel: Symbol('GESTURES.PINCH_CANCEL'),
  PinchIn: Symbol('GESTURES.PINCH_IN'),
  PinchOut: Symbol('GESTURES.PINCH_OUT'),

  Press: Symbol('GESTURES.PRESS'),
  PressUp: Symbol('GESTURES.PRESS_UP'),

  Rotate: Symbol('GESTURES.ROTATE'),
  RotateStart: Symbol('GESTURES.ROTATE_START'),
  RotateMove: Symbol('GESTURES.ROTATE_MOVE'),
  RotateEnd: Symbol('GESTURES.ROTATE_END'),
  RotateCancel: Symbol('GESTURES.ROTATE_CANCEL'),

  Swipe: Symbol('GESTURES.SWIPE'),
  SwipeLeft: Symbol('GESTURES.SWIPE_LEFT'),
  SwipeRight: Symbol('GESTURES.SWIPE_RIGHT'),
  SwipeUp: Symbol('GESTURES.SWIPE_UP'),
  SwipeDown: Symbol('GESTURES.SWIPE_DOWN'),

  Tap: Symbol('GESTURES.TAP'),
};

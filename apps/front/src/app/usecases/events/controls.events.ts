export const ControlsEvents = {
  ThemeChange: Symbol('CONTROLS.THEME_CHANGE'),

  NavigationStateChange: Symbol('CONTROLS.NAVIGATION_STATE_CHANGE'),
};

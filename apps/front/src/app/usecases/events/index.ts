import { EventEmitter, Injectable } from '@angular/core';

import { GesturesEvent } from './gestures.event';
import { ControlsEvents } from './controls.events';

@Injectable({
  providedIn: 'root',
})
export class EventBus {
  private readonly emitters: { [key: EventSymbol]: EventEmitter<any> } =
    Object.fromEntries(
      EventSymbols.map((event: EventSymbol) => {
        return [event, new EventEmitter<any>()];
      })
    );

  subscribe<T>(event: EventSymbol, listener: EventListener<T>) {
    this.emitters[event].subscribe(listener);
  }

  unsubscribe(event: EventSymbol) {
    this.emitters[event].unsubscribe();
  }

  emit(event: EventSymbol, ...args: any[]) {
    this.emitters[event].emit(...args);
  }
}

export const EventSymbols: symbol[] = [
  Object.values<symbol>(GesturesEvent),
  Object.values<symbol>(ControlsEvents),
].flat();
export type EventSymbol = (typeof EventSymbols)[number];

type EventListener<T> = (payload: T) => boolean | void;

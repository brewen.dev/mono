import { createReducer, on } from '@ngrx/store';

import { ControlsActions } from './controls.actions';
import { ControlsInitialState, ControlsState } from './controls.state';

export const ControlsReducer = createReducer(
  ControlsInitialState,
  on(
    ControlsActions.setTheme,
    (state: ControlsState, { theme }): ControlsState => ({
      ...state,
      theme,
    })
  ),
  on(
    ControlsActions.toggleNavigation,
    (state: ControlsState, { collapsed }): ControlsState => ({
      ...state,
      navigationCollapsed: collapsed,
    })
  )
);

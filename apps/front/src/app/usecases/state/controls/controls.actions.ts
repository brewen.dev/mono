import { createActionGroup, props } from '@ngrx/store';

import { Theme } from '@domain/constants/global.const';

export const ControlsActions = createActionGroup({
  source: 'Controls',
  events: {
    'Set Theme': props<{ theme: Theme }>(),
    'Toggle Navigation': props<{ collapsed: boolean }>(),
  },
});

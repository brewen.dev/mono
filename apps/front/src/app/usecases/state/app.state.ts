import { ControlsInitialState, ControlsState } from './controls/controls.state';

export interface State {
  controls: ControlsState;
}

export const InitialState: Readonly<State> = {
  controls: ControlsInitialState,
};

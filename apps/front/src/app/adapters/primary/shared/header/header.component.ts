import { Component } from '@angular/core';
import { faBars } from '@fortawesome/free-solid-svg-icons';

import { TITLE } from '@domain/constants/global.const';

import { EventBus } from '@usecases/events';

@Component({
  selector: 'bdev-header',
  templateUrl: './header.template.html',
  styleUrls: ['./header.style.scss'],
})
export class HeaderComponent {
  protected readonly _title: string = TITLE;
  protected readonly _faBars = faBars;

  constructor(private readonly eventBus: EventBus) {}
}

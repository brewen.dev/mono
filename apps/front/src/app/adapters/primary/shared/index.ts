import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgClass, NgIf } from '@angular/common';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { HeaderComponent } from './header/header.component';

const Components = [HeaderComponent];

@NgModule({
  imports: [FontAwesomeModule, RouterModule, NgIf, NgClass],
  declarations: Components,
  exports: Components,
  providers: [
    {
      provide: Window,
      useValue: window,
    },
  ],
})
export class AdaptersPrimarySharedModule {}

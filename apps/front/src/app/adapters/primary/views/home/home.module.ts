import { NgModule } from '@angular/core';

import { HomeComponent } from './home.component';
import { AdaptersPrimarySharedModule } from '@adapters/primary/shared';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [FontAwesomeModule, AdaptersPrimarySharedModule],
  declarations: [HomeComponent],
})
export class HomeModule {}

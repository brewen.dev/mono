import { Component } from '@angular/core';
import { faBars } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'bdev-home',
  templateUrl: './home.template.html',
  styleUrls: ['./home.style.scss'],
})
export class HomeComponent {
  _faBars = faBars;
}

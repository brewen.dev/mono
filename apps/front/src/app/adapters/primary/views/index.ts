import { NgModule } from '@angular/core';

import { HomeModule } from './home/home.module';
import { NotFoundModule } from './not-found/not-found.module';

@NgModule({
  imports: [HomeModule, NotFoundModule],
  exports: [HomeModule, NotFoundModule],
})
export class ViewsModule {}

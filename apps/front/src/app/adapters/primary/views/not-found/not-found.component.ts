import { Component } from '@angular/core';

@Component({
  selector: 'bdev-404',
  templateUrl: './not-found.template.html',
  styleUrls: ['./not-found.style.scss'],
})
export class NotFoundComponent {}

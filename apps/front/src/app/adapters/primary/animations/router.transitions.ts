import {
  style,
  animate,
  query as q,
  group,
  trigger,
  transition,
} from '@angular/animations';

const query = (style: any, animate: any, optional = { optional: true }) =>
  q(style, animate, optional);

const fade = [
  query(':enter, :leave', style({ position: 'fixed', width: '100%' })),
  query(':enter', [style({ opacity: 0 })]),
  group([
    query(':leave', [animate('0.3s ease-out', style({ opacity: 0 }))]),
    query(':enter', [
      style({ opacity: 0 }),
      animate('0.3s ease-out', style({ opacity: 1 })),
    ]),
  ]),
];

const fadeInFromDirection = (direction: RouterTransitionDirection) => [
  query(':enter, :leave', style({ position: 'fixed', width: '100%' })),
  group([
    query(':enter', [
      style({
        transform: `translateX(${
          direction === RouterTransitionDirection.Backward ? '-' : ''
        }15%)`,
        opacity: 0,
      }),
      animate(
        '0.3s ease-out',
        style({ transform: 'translateX(0%)', opacity: 1 })
      ),
    ]),
    query(':leave', [
      style({ transform: 'translateX(0%)' }),
      animate('0.3s ease-out', style({ opacity: 0 })),
    ]),
  ]),
];

export enum RouterTransitionDirection {
  None = 'none',
  Initial = 'initial',
  Section = 'section',
  Forward = 'forward',
  Backward = 'backward',
}

export const routerTransition = trigger('routerTransition', [
  transition(`* => ${RouterTransitionDirection.Initial}`, fade),
  transition(`* => ${RouterTransitionDirection.Section}`, fade),
  transition(
    `* => ${RouterTransitionDirection.Forward}`,
    fadeInFromDirection(RouterTransitionDirection.Forward)
  ),
  transition(
    `* => ${RouterTransitionDirection.Backward}`,
    fadeInFromDirection(RouterTransitionDirection.Backward)
  ),
]);

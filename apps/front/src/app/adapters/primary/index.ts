import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AdaptersPrimarySharedModule } from './shared';
import { ViewsModule } from './views';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FontAwesomeModule,

    AdaptersPrimarySharedModule,
    ViewsModule,
  ],
  exports: [AdaptersPrimarySharedModule, ViewsModule],
})
export class AdaptersPrimaryModule {}

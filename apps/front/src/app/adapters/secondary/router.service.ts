import { Injectable } from '@angular/core';
import { RouterOutlet } from '@angular/router';

import { RouterTransitionDirection } from '@adapters/primary/animations/router.transitions';

@Injectable({
  providedIn: 'root',
})
export class RouterService {
  private _outlet!: RouterOutlet;
  private _previousPath = '';

  public pageTransition: RouterTransitionDirection =
    RouterTransitionDirection.None;

  private _bindRouterOutletEvents() {
    this._outlet.activateEvents.subscribe(() => {
      this._getPageTransition();
    });
  }
  private _unbindRouterOutletEvents() {
    this._outlet?.activateEvents?.unsubscribe();
  }

  private _getPageTransition() {
    if (this._outlet.isActivated) {
      let transitionName = RouterTransitionDirection.Section;

      const route = this._outlet.activatedRoute.routeConfig;
      const path = route?.path as string;
      const isSame = path === this._previousPath;
      const isBackward = this._previousPath.startsWith(path);
      const isForward = path.startsWith(this._previousPath);

      if (isSame) {
        transitionName = RouterTransitionDirection.None;
      } else if (isBackward && isForward) {
        transitionName = RouterTransitionDirection.Initial;
      } else if (isBackward) {
        transitionName = RouterTransitionDirection.Backward;
      } else if (isForward) {
        transitionName = RouterTransitionDirection.Forward;
      }

      this._previousPath = path;

      this.pageTransition = transitionName;
    }
  }

  set outlet(outlet: RouterOutlet) {
    this._unbindRouterOutletEvents();
    this._outlet = outlet;
    this._bindRouterOutletEvents();
  }
}

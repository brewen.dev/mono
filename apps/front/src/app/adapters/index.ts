import { NgModule } from '@angular/core';

import { AdaptersPrimaryModule } from './primary';

@NgModule({
  imports: [AdaptersPrimaryModule],
  exports: [AdaptersPrimaryModule],
})
export class AdaptersModule {}

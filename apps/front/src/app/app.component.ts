import {
  AfterViewInit,
  Component,
  HostListener,
  OnInit,
  ViewChild,
} from '@angular/core';
import { RouterOutlet } from '@angular/router';

import { Theme } from '@domain/constants/global.const';

import { EventBus } from '@usecases/events';
import { GesturesEvent } from '@usecases/events/gestures.event';

import { routerTransition } from '@adapters/primary/animations/router.transitions';
import { RouterService } from '@adapters/secondary/router.service';
import { ControlsEvents } from '@usecases/events/controls.events';

@Component({
  selector: 'bdev-root',
  templateUrl: './app.template.html',
  styleUrls: ['./app.style.scss'],
  animations: [routerTransition],
})
export class AppComponent implements OnInit, AfterViewInit {
  constructor(
    private readonly eventBus: EventBus,
    protected readonly routerService: RouterService
  ) {}

  @ViewChild('routerOutlet', { static: true }) routerOutlet!: RouterOutlet;

  ngOnInit() {
    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    prefersDark.addListener((mediaQuery) => this.setColorScheme(mediaQuery));

    this.eventBus.subscribe<Theme>(ControlsEvents.ThemeChange, (theme: Theme) =>
      this.setTheme(theme)
    );

    this.routerService.outlet = this.routerOutlet;
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const loader = document.getElementById('app-loader');
      if (loader) {
        loader.classList.add('loaded');

        setTimeout(() => {
          loader.remove();
        }, 300);
      }
    }, 500);
  }

  // Theme
  setColorScheme(mediaQuery: MediaQueryListEvent | MediaQueryList) {
    const isDark = mediaQuery.matches;
    this.eventBus.emit(
      ControlsEvents.ThemeChange,
      isDark ? Theme.Dark : Theme.Light
    );
  }
  setTheme(theme: Theme) {
    this.theme = theme;
  }
  private _theme: Theme = Theme.Light;
  get theme(): Theme {
    return this._theme;
  }
  set theme(theme: Theme) {
    this._theme = theme;
    document.body.classList.remove(Theme.Light, Theme.Dark);
    document.body.classList.add(theme);
  }

  // Global listeners
  @HostListener('swipe')
  onSwipe = () => {
    this.eventBus.emit(GesturesEvent.Swipe);
  };
  @HostListener('swipeleft')
  onSwipeLeft = () => {
    this.eventBus.emit(GesturesEvent.SwipeLeft);
  };
  @HostListener('swiperight')
  onSwipeRight = () => {
    this.eventBus.emit(GesturesEvent.SwipeRight);
  };
  @HostListener('swipeup')
  onSwipeUp = () => {
    this.eventBus.emit(GesturesEvent.SwipeUp);
  };
  @HostListener('swipedown')
  onSwipeDown = () => {
    this.eventBus.emit(GesturesEvent.SwipeDown);
  };

  @HostListener('pan', ['$event'])
  onPan = ($event: any) => {
    this.eventBus.emit(GesturesEvent.Pan, $event);
  };
  @HostListener('panstart', ['$event'])
  onPanStart = ($event: any) => {
    this.eventBus.emit(GesturesEvent.PanStart, {
      cursor: $event.pointerType,
      x: $event.center.x,
      y: $event.center.y,
    });
  };
  @HostListener('panend', ['$event'])
  onPanEnd = ($event: any) => {
    this.eventBus.emit(GesturesEvent.PanEnd, {
      cursor: $event.pointerType,
      x: $event.center.x,
      y: $event.center.y,
    });
  };
  @HostListener('panmove', ['$event'])
  onPanMove = ($event: any) => {
    this.eventBus.emit(GesturesEvent.PanMove, {
      cursor: $event.pointerType,
      x: $event.deltaX,
      y: $event.deltaY,
    });
  };
}

/**
 * Interface of the simple literal object with any string keys.
 */
export interface ObjectLiteral {
  [key: string]: unknown;
}

export interface SymbolObject {
  [key: string]: symbol;
}

export interface TypedObject<T> {
  [key: string]: T;
}

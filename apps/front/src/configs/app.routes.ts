import { ExtraOptions, Routes } from '@angular/router';

import { HomeComponent } from '@adapters/primary/views/home/home.component';
import { NotFoundComponent } from '@adapters/primary/views/not-found/not-found.component';

export const AppRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent,
    title: 'Home',
  },
  {
    path: '**',
    component: NotFoundComponent,
    title: 'Not Found',
  },
];

export const RoutesConfig: ExtraOptions = {
  useHash: false,
  initialNavigation: 'enabledBlocking',
};
